<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LrInvoiceFiled extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lr_invoice_filed', function (Blueprint $table) {
            $table->id();
            $table->string('trip_id');
            $table->string('trip_assign_id');
            $table->string('invoice_no');
            $table->string('e_waybill');
            $table->string('box_count');
            $table->string('invoice_val');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lr_invoice_filed');
    }
}
