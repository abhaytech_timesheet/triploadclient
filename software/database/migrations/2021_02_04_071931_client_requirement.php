<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClientRequirement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_requirement', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('client_id');
            $table->string('trip_id')->unique();
            $table->bigInteger('p_location_id');
            $table->string('p_location');
            $table->string('p_address');
            $table->Integer('p_state');
            $table->Integer('p_city');
            $table->string('p_landmark');
            $table->string('p_contact_person');
            $table->string('p_mob_no');
            $table->date('p_date')->useCurrent();
            $table->time('p_time')->useCurrent();

            $table->bigInteger('d_location_id');
            $table->string('d_location');
            $table->string('d_address');
            $table->string('d_delivery_points');
            $table->Integer('d_state');
            $table->Integer('d_city');
            $table->string('d_landmark');
            $table->string('d_contact_person');
            $table->string('d_mob_no');
            $table->date('d_date')->useCurrent();
            $table->time('d_time')->useCurrent();
            
            $table->string('total_weight');
            $table->Integer('truck_type'); /* vehicle_type*/
            $table->enum('shipment_type', ['Fragile', 'Perishable', 'Standard Shipping']);
            $table->enum('freight_type', ['FTL', 'FTL Home Del.', 'Ecom']);
            $table->text('material_desc');
            $table->string('gps');
            $table->string('loading_unloading');

            $table->string('created_by');
            $table->string('updated_by');
            $table->string('floated_range_min')->nullable();
            $table->string('floated_range_max')->nullable();
            $table->string('agreed_sales_value')->nullable();
            $table->string('vendor_purchase_value')->nullable();
            $table->Integer('vehicle_type_id')->nullable();
            $table->Integer('alloted_to_vendor_id')->nullable();
            $table->text('comments')->nullable();
            $table->enum('trip_status', ['Approved', 'Pending', 'Declined','Processing']);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_requirement');
    }
}
