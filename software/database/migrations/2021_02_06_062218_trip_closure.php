<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TripClosure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Trip_Closure', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('trip_id');
            $table->string('lr_number');
            $table->string('lr_file');
            $table->string('lr_date');
            $table->string('ewaybill_number');
            $table->string('ewaybill_file');
            $table->string('from_address');
            $table->string('to_address');
            $table->string('vehicle_number');
            $table->string('count_of_shipment');
            $table->string('invoice');
            $table->string('POD')->nullable();
            $table->string('POD_file')->nullable();
            $table->text('damages')->nullable();
            $table->text('damages_file')->nullable();
            $table->string('fine')->nullable();
            $table->string('fine_file')->nullable();
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Trip_Closure');
    }
}
