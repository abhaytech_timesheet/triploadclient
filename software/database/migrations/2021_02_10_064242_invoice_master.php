<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InvoiceMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_master', function (Blueprint $table) {
            $table->id();
            $table->Integer('trip_id');
            $table->string('invoice_no');
            $table->string('e_way_bill');
            $table->string('place_of_supp');
            $table->string('lr_no');
            $table->text('bill_to')->nullable();
            $table->timestamp('due_date')->useCurrent();
            $table->string('corporation')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('status')->nullable();
            $table->Integer('approve_status')->nullable();
            $table->Integer('pod_received')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->string('approve_by')->nullable();
            $table->string('client_invoice_img')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_master');
    }
}
