<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Client extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('enterprise_name');
            $table->string('poc');
            $table->longText('address');
            $table->enum('organization_type', ['Public Ltd', 'Private Ltd', 'LLP']);
            $table->enum('delivery_type', ['FTL', 'FTL Home Del.', 'Ecom']);
            $table->enum('frequency', ['One Time', 'Monthly']);
            $table->string('mobile_number');
            $table->string('email')->nullable();
            $table->string('gst')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
