<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TripAssignments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_assignments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('client_id');
            $table->Integer('trip_id');
            $table->string('elr_num')->unique();
            $table->text('vehicle_type_id');
            $table->text('driver_id');
            $table->date('tentative_date');
            $table->time('tentative_time');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_assignments');
    }
}
