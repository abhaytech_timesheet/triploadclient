<?php

namespace App\Providers;
use DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Location\City\CityRepository;
use App\Repositories\Location\City\CityEloquent;
use App\Repositories\Location\States\StatesRepository;
use App\Repositories\Location\States\StatesEloquent;
use App\Repositories\Location\Zones\ZonesRepository;
use App\Repositories\Location\Zones\ZonesEloquent;
use App\Repositories\Location\Loc\LocRepository;
use App\Repositories\Location\Loc\LocEloquent;
use App\Repositories\Client\Creation\CreationRepository;
use App\Repositories\Client\Creation\CreationEloquent;
use App\Repositories\Client\Requirement\RequirementRepository;
use App\Repositories\Client\Requirement\RequirementEloquent;
use App\Repositories\Client\DeliveryPoints\DeliveryPointsRepository;
use App\Repositories\Client\DeliveryPoints\DeliveryPointsEloquent;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserEloquent;
use App\Repositories\Affiliate\AffiliateRepository;
use App\Repositories\Affiliate\AffiliateEloquent;
use App\Repositories\POC\POCRepository;
use App\Repositories\POC\POCEloquent;
use App\Repositories\Trip\TripRepository;
use App\Repositories\Trip\TripEloquent;
use App\Repositories\VehicleType\VehicleTypeRepository;
use App\Repositories\VehicleType\VehicleTypeEloquent;
use App\Repositories\Vendor\VendorMaster\VendorMasterRepository;
use App\Repositories\Vendor\VendorMaster\VendorMasterEloquent;
use App\Repositories\TripAssign\TripAssignRepository;
use App\Repositories\TripAssign\TripAssignEloquent;
use App\Repositories\TripImg\TripImgRepository;
use App\Repositories\TripImg\TripImgEloquent;
use App\Repositories\TripClosure\TripClosureRepository;
use App\Repositories\TripClosure\TripClosureEloquent;
use App\Repositories\ELR\ELRRepository;
use App\Repositories\ELR\ELREloquent;
use App\Repositories\Invoice\InvoiceMaster\InvoiceMasterRepository;
use App\Repositories\Invoice\InvoiceMaster\InvoiceMasterEloquent;
use App\Repositories\Invoice\InvoiceDescription\InvoiceDescriptionRepository;
use App\Repositories\Invoice\InvoiceDescription\InvoiceDescriptionEloquent;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CityRepository::class, CityEloquent::class);
        $this->app->singleton(StatesRepository::class, StatesEloquent::class);
        $this->app->singleton(ZonesRepository::class, ZonesEloquent::class);
        $this->app->singleton(CreationRepository::class, CreationEloquent::class);
        $this->app->singleton(RequirementRepository::class, RequirementEloquent::class);
        $this->app->singleton(DeliveryPointsRepository::class, DeliveryPointsEloquent::class);
        $this->app->singleton(UserRepository::class, UserEloquent::class);
        $this->app->singleton(VehicleTypeRepository::class, VehicleTypeEloquent::class);
        $this->app->singleton(AffiliateRepository::class, AffiliateEloquent::class);
        $this->app->singleton(POCRepository::class, POCEloquent::class);
        $this->app->singleton(TripRepository::class, TripEloquent::class);
        $this->app->singleton(LocRepository::class, LocEloquent::class);
        $this->app->singleton(VendorMasterRepository::class, VendorMasterEloquent::class);
        $this->app->singleton(TripAssignRepository::class, TripAssignEloquent::class);
        $this->app->singleton(TripImgRepository::class, TripImgEloquent::class);
        $this->app->singleton(TripClosureRepository::class, TripClosureEloquent::class);
        $this->app->singleton(InvoiceMasterRepository::class, InvoiceMasterEloquent::class);
        $this->app->singleton(InvoiceDescriptionRepository::class, InvoiceDescriptionEloquent::class);
        $this->app->singleton(ELRRepository::class, ELREloquent::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Schema::defaultStringLength(191);
    }
}
