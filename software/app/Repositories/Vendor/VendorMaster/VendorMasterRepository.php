<?php

namespace App\Repositories\Vendor\VendorMaster;

interface VendorMasterRepository
{
    public function GetAll();

    public function GetAllWithNem();

    public function GetById($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


