<?php

namespace App\Repositories\Vendor\VendorMaster;

use DB;

class VendorMasterEloquent implements VendorMasterRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('vendor_master')->orderby('id','desc')->get();
    }

    public function GetAllWithNem()
    {
        return  DB::table('vendor_master')
        ->leftjoin('users','vendor_master.vendor_id','=','users.id')
        ->select('vendor_master.id','vendor_master.vendor_id','users.name')->orderby('users.name','asc')->get();
    }


    public function GetById($id)
    {   
        return DB::table('vendor_master')->where('id', $id)->first();
    }

    public function Create($data)
    {
        return DB::table('vendor_master')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('vendor_master')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('vendor_master')->where('id', $id)->delete();
    }

}




