<?php

namespace App\Repositories\VehicleType;

use DB;

class VehicleTypeEloquent implements VehicleTypeRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('vehicle_type')->orderby('id','desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('vehicle_type')->where('id', $id)->first();
    }

    public function GetByMobile($no)
    {   
        return DB::table('vehicle_type')->orderby('id','desc')->where('mobile_no', $no)->first();
    }

    public function Create($data)
    {
        return DB::table('vehicle_type')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('vehicle_type')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('vehicle_type')->where('id', $id)->delete();
    }

}




