<?php

namespace App\Repositories\User;

use DB;

class UserEloquent implements UserRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('users')->orderby('id','desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('users')->where('id', $id)->first();
    }

    public function GetByMobile($no)
    {   
        return DB::table('users')->orderby('id','desc')->where('mobile_no', $no)->first();
    }

    public function Create($data)
    {
        return DB::table('users')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('users')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('users')->where('id', $id)->delete();
    }

}




