<?php

namespace App\Repositories\Invoice\InvoiceDescription;

use DB;

class InvoiceDescriptionEloquent implements InvoiceDescriptionRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('invoice_description')->orderby('id','desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('invoice_description')->where('id', $id)->first();
    }

    public function GetByInId($id)
    {   
        return DB::table('invoice_description')->where('invoice_id', $id)->get();
    }

    public function Create($data)
    {
        return DB::table('invoice_description')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('invoice_description')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('invoice_description')->where('id', $id)->delete();
    }

}




