<?php

namespace App\Repositories\Invoice\InvoiceDescription;

interface InvoiceDescriptionRepository
{
    public function GetAll();

    public function GetById($id);

    public function GetByInId($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


