<?php

namespace App\Repositories\Invoice\InvoiceMaster;

use DB;

class InvoiceMasterEloquent implements InvoiceMasterRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('invoice_master')->orderby('id','desc')->get();
    }


    public function GetAllWithTrip()
    {
        return DB::table('invoice_master')->leftjoin('client_requirement','invoice_master.trip_id','=','client_requirement.id')->orderby('invoice_master.id','desc')->select('invoice_master.id as in_id','invoice_master.created_by as in_created_by','invoice_master.updated_by as in_updated_by','invoice_master.updated_at as in_updated_at','invoice_master.created_at as in_created_at','invoice_master.*','client_requirement.*')->get();
    }



    public function GetByIdWithTrip($id)
    {
        return DB::table('invoice_master')->leftjoin('client_requirement','invoice_master.trip_id','=','client_requirement.id')->orderby('invoice_master.id','desc')->select('invoice_master.id as in_id','invoice_master.created_by as in_created_by','invoice_master.updated_by as in_updated_by','invoice_master.updated_at as in_updated_at','invoice_master.created_at as in_created_at','invoice_master.*','client_requirement.*')->where('client_requirement.id', $id)->get();
    }

    public function GetById($id)
    {   
        return DB::table('invoice_master')->where('id', $id)->first();
    }

    public function Create($data)
    {
        return DB::table('invoice_master')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('invoice_master')->where('id', $id)->update($data);
    }

    public function UpdateByTrip($id, $data)
    {
        return DB::table('invoice_master')->where('trip_id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('invoice_master')->where('id', $id)->delete();
    }

}




