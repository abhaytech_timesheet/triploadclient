<?php

namespace App\Repositories\Invoice\InvoiceMaster;

interface InvoiceMasterRepository
{
    public function GetAll();

	public function GetAllWithTrip();

	public function GetByIdWithTrip($id);

    public function GetById($id);

    public function Create($data);

    public function Update($id, $data);

    public function UpdateByTrip($id, $data);

    public function Delete($id);
}


