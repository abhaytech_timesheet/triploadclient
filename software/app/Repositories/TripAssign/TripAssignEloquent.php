<?php

namespace App\Repositories\TripAssign;

use DB;

class TripAssignEloquent implements TripAssignRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('trip_assignments')->orderby('id','desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('trip_assignments')->where('id', $id)->first();
    }

    public function GetByTId($id)
    {   
        return DB::table('trip_assignments')->where('trip_id', $id)->first();
    }


    public function Create($data)
    {
        return DB::table('trip_assignments')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('trip_assignments')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('trip_assignments')->where('id', $id)->delete();
    }


    public function GetByDriverId($id)
    {   
        return DB::table('trip_assignments')->where('driver_id', $id)->get();
    }

}




