<?php

namespace App\Repositories\TripAssign;

interface TripAssignRepository
{
    public function GetAll();

    public function GetById($id);
    
    public function GetByTId($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);

    public function GetByDriverId($id);
}


