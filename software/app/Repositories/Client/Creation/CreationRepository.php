<?php

namespace App\Repositories\Client\Creation;

interface CreationRepository
{
    public function GetAll();

    public function GetById($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


