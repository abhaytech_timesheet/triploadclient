<?php

namespace App\Repositories\Client\Creation;

use DB;

class CreationEloquent implements CreationRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('clients')->orderby('id', 'desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('clients')->where('id', $id)->first();
    }

    public function Create($data)
    {
        return DB::table('clients')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('clients')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('clients')->where('id', $id)->delete();
    }

}




