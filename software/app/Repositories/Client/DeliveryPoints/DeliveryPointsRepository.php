<?php

namespace App\Repositories\Client\DeliveryPoints;

interface DeliveryPointsRepository
{
    public function GetAll();

    public function GetById($id);

    public function GetByRId($id);

    public function GetByTId($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


