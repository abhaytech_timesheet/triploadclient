<?php

namespace App\Repositories\Client\DeliveryPoints;

use DB;

class DeliveryPointsEloquent implements DeliveryPointsRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('delivery_points')->orderby('id', 'desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('delivery_points')->where('id', $id)->first();
    }

    
    public function GetByRId($id)
    {   
        return DB::table('delivery_points')->where('client_requirement_id', $id)->get();
    }

        
    public function GetByTId($id)
    {   
        return DB::table('delivery_points')->where('trip_id', $id)->get();
    }

    public function Create($data)
    {
        return DB::table('delivery_points')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('delivery_points')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('delivery_points')->where('id', $id)->delete();
    }

}




