<?php

namespace App\Repositories\Client\Requirement;

use DB;

class RequirementEloquent implements RequirementRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('client_requirement')->orderby('id', 'desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('client_requirement')->where('id', $id)->first();
    }

    public function Create($data)
    {
        return DB::table('client_requirement')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('client_requirement')->where('id', $id)->update($data);
    }

        
    public function GetByCreateTrip()
    {   
        return DB::table('client_requirement')->where('vendor_purchase_value', '!=', 'null')->get();
    }


    public function UpdateByTrip($id, $data)
    {
        return DB::table('client_requirement')->where('trip_id', $id)->update($data);
    }

    public function GetByTrip($id)
    {   
        return DB::table('client_requirement')->where('trip_id', $id)->first();
    }


    public function GetByTripIds($id)
    {   
        return DB::table('client_requirement')->leftjoin('trip_closure','client_requirement.id','=','trip_closure.trip_id')->leftjoin('invoice_master','client_requirement.id','=','invoice_master.trip_id')->orderby('client_requirement.id','desc')->select('client_requirement.*','trip_closure.ewaybill_file','trip_closure.lr_file','invoice_master.client_invoice_img')->whereIn('client_requirement.id', $id)->get();
    }


    public function Delete($id)
    {
        return DB::table('client_requirement')->where('id', $id)->delete();
    }

}




