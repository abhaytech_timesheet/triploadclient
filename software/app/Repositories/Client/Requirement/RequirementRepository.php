<?php

namespace App\Repositories\Client\Requirement;

interface RequirementRepository
{
    public function GetAll();

    public function GetById($id);

    public function Create($data);

    public function Update($id, $data);

    public function GetByCreateTrip();

    public function UpdateByTrip($id, $data);

    public function GetByTrip($id);

    public function Delete($id);
}


