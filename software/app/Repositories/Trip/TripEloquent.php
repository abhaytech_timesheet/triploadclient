<?php

namespace App\Repositories\Trip;

use DB;

class TripEloquent implements TripRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('trips')->get();
    }

    public function GetById($id)
    {   
        return DB::table('trips')->where('id', $id)->first();
    }

    public function Create($data)
    {
        return DB::table('trips')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('trips')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('trips')->where('id', $id)->delete();
    }

}




