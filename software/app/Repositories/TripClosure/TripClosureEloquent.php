<?php

namespace App\Repositories\TripClosure;

use DB;

class TripClosureEloquent implements TripClosureRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('trip_closure')->orderby('id','desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('trip_closure')->where('id', $id)->first();
    }

    public function GetByTId($id)
    {   
        return DB::table('trip_closure')->where('trip_id', $id)->first();
    }


    public function GetByTIds($id)
    {   
        return DB::table('trip_closure')->whereIn('trip_id', $id)->get();
    }


    public function Create($data)
    {
        return DB::table('trip_closure')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('trip_closure')->where('id', $id)->update($data);
    }

    public function UpdateByTrip($id, $data)
    {
        return DB::table('trip_closure')->where('trip_id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('trip_closure')->where('id', $id)->delete();
    }

}




