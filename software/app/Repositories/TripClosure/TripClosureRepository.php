<?php

namespace App\Repositories\TripClosure;

interface TripClosureRepository
{
    public function GetAll();

    public function GetById($id);
    
    public function GetByTId($id);

    public function GetByTIds($id);

    public function Create($data);

    public function Update($id, $data);

    public function UpdateByTrip($id, $data);

    public function Delete($id);
}


