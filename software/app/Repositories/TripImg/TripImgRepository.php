<?php

namespace App\Repositories\TripImg;

interface TripImgRepository
{
    public function GetAll();

    public function GetById($id);
    
    public function GetByTId($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


