<?php

namespace App\Repositories\TripImg;

use DB;

class TripImgEloquent implements TripImgRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('trip_image')->orderby('id','desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('trip_image')->where('id', $id)->first();
    }

    public function GetByTId($id)
    {   
        return DB::table('trip_image')->where('trip_id', $id)->first();
    }


    public function Create($data)
    {
        return DB::table('trip_image')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('trip_image')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('trip_image')->where('id', $id)->delete();
    }

}




