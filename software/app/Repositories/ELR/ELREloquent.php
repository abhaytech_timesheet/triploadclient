<?php

namespace App\Repositories\ELR;

use DB;

class ELREloquent implements ELRRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('lr_invoice_filed')->orderby('id','desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('lr_invoice_filed')->where('id', $id)->first();
    }

    public function GetByTId($id)
    {   
        return DB::table('lr_invoice_filed')->where('trip_id', $id)->get();
    }


    public function Create($data)
    {
        return DB::table('lr_invoice_filed')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('lr_invoice_filed')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('lr_invoice_filed')->where('id', $id)->delete();
    }

}




