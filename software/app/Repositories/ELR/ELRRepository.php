<?php

namespace App\Repositories\ELR;

interface ELRRepository
{
    public function GetAll();

    public function GetById($id);
    
    public function GetByTId($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


