<?php

namespace App\Repositories\Affiliate;

interface AffiliateRepository
{
    public function GetAll();

    public function GetById($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


