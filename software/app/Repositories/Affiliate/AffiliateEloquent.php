<?php

namespace App\Repositories\Affiliate;

use DB;

class AffiliateEloquent implements AffiliateRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('affiliates')->get();
    }

    public function GetById($id)
    {   
        return DB::table('affiliates')->where('id', $id)->first();
    }


    public function Create($data)
    {
        return DB::table('affiliates')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('affiliates')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('affiliates')->where('id', $id)->delete();
    }

}




