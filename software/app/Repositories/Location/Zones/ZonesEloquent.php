<?php

namespace App\Repositories\Location\Zones;

use DB;

class ZonesEloquent implements ZonesRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('zones')->get();
    }

    public function GetById($id)
    {   
        return DB::table('zones')->where()->first();
    }


    public function Create($data)
    {
        return DB::table('zones')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('zones')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('zones')->where('id', $id)->delete();
    }


}




