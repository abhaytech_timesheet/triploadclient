<?php

namespace App\Repositories\Location\City;

use DB;

class CityEloquent implements CityRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('city')->get();
    }

    public function GetById($id)
    {   
        return DB::table('city')->where('id', $id)->first();
    }

    public function GetByState($id)
    {   
        return DB::table('city')->where('state_id', $id)->get();
    }

    public function Create($data)
    {
        return DB::table('city')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('city')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('city')->where('id', $id)->delete();
    }

}




