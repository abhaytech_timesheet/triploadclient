<?php

namespace App\Repositories\Location\City;

interface CityRepository
{
    public function GetAll();

    public function GetById($id);

    public function GetByState($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


