<?php

namespace App\Repositories\Location\Loc;

interface LocRepository
{
    public function GetAll();

    public function GetById($id);

    public function SearchByName($name);

    public function GetByidandComp($id);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


