<?php

namespace App\Repositories\Location\Loc;

use DB;

class LocEloquent implements LocRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('location_masters')->get();
    }

    public function GetById($id)
    {   
        return DB::table('location_masters')->where('id', $id)->first();
    }


    public function GetByidandComp($id)
    {   
        return DB::table('location_masters')->leftjoin('company_masters','location_masters.company_id','=','company_masters.id')->where('location_masters.id', $id)->select('location_masters.id','location_masters.type','company_masters.company_name')->first();
    }

    public function SearchByName($name)
    {   
        return  DB::table('location_masters')
        ->leftjoin('company_masters','location_masters.company_id','=','company_masters.id')
        ->orderby('location_masters.type','asc')->where('location_masters.type','LIKE','%'.$name.'%')->select('location_masters.id','location_masters.type','company_masters.company_name')->take(4)->get();
    }

    public function Create($data)
    {
        return DB::table('location_masters')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('location_masters')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('location_masters')->where('id', $id)->delete();
    }


}




