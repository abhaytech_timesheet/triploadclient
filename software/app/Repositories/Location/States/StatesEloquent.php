<?php

namespace App\Repositories\Location\States;

use DB;

class StatesEloquent implements StatesRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('states')->get();
    }

    public function GetById($id)
    {   
        return DB::table('states')->where('id', $id)->first();
    }


    public function Create($data)
    {
        return DB::table('states')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('states')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('states')->where('id', $id)->delete();
    }

}




