<?php

namespace App\Repositories\POC;

interface POCRepository
{
    public function GetAll();

    public function GetById($id);

    public function GetByMobile($no);

    public function Create($data);

    public function Update($id, $data);

    public function Delete($id);
}


