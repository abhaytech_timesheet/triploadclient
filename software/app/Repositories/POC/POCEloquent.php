<?php

namespace App\Repositories\POC;

use DB;

class POCEloquent implements POCRepository
{
    private $model;

    public function __construct(DB $model)
    {
        $this->model = $model;
    }

    public function GetAll()
    {
        return DB::table('poc')->orderby('id','desc')->get();
    }

    public function GetById($id)
    {   
        return DB::table('poc')->where('id', $id)->first();
    }

    public function GetByMobile($no)
    {   
        return DB::table('poc')->orderby('id','desc')->where('mobile_no', $no)->first();
    }

    public function Create($data)
    {
        return DB::table('poc')->insert($data);
    }

    public function Update($id, $data)
    {
        return DB::table('poc')->where('id', $id)->update($data);
    }

    public function Delete($id)
    {
        return DB::table('poc')->where('id', $id)->delete();
    }

}




