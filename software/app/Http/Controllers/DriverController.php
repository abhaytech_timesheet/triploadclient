<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Location\City\CityRepository;
use App\Repositories\Location\States\StatesRepository;
use App\Repositories\Location\Zones\ZonesRepository;
use App\Repositories\Client\Creation\CreationRepository;
use App\Repositories\Client\Requirement\RequirementRepository;
use App\Repositories\Client\DeliveryPoints\DeliveryPointsRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\VehicleType\VehicleTypeRepository;
use App\Repositories\Affiliate\AffiliateRepository;
use App\Repositories\POC\POCRepository;
use App\Repositories\POC\TripRepository;
use App\Repositories\Vendor\VendorMaster\VendorMasterRepository;
use App\Repositories\TripAssign\TripAssignRepository;
use App\Repositories\Location\Loc\LocRepository;
use App\Repositories\TripImg\TripImgRepository;
use App\Repositories\TripClosure\TripClosureRepository;
use App\Repositories\ELR\ELRRepository;
use App\Repositories\Invoice\InvoiceDescription\InvoiceDescriptionRepository;
use App\Repositories\Invoice\InvoiceMaster\InvoiceMasterRepository;
use Hash;
use DB;
use PDF;
use Image;
use Carbon\Carbon;

class DriverController extends Controller
{
 	private $CityRepo;
	private $StatesRepo;
	private $ZonesRepo;
    private $CreationRepo;
    private $UserRepo;
    private $AffiliateRepo;
    private $POCRepo;
    private $RequirementRepo;
    private $VehicleTypeRepo;
    private $DeliveryPointsRepo;
    private $VendorMasterRepo;
    private $TripAssignRepo;
    private $TripImgRepo;
    private $TripClosureRepo;
    private $LocRepo;
    private $InvoiceDescriptionRepo;
    private $InvoiceMasterRepo;
    private $ELRRepo;
    
    public function __construct(CityRepository $CityRepo, StatesRepository $StatesRepo, ZonesRepository $ZonesRepo, CreationRepository $CreationRepo, UserRepository $UserRepo, AffiliateRepository $AffiliateRepo, POCRepository $POCRepo, RequirementRepository $RequirementRepo, VehicleTypeRepository $VehicleTypeRepo, DeliveryPointsRepository $DeliveryPointsRepo,  VendorMasterRepository $VendorMasterRepo, TripAssignRepository $TripAssignRepo, TripImgRepository $TripImgRepo , TripClosureRepository $TripClosureRepo, LocRepository $LocRepo, InvoiceDescriptionRepository $InvoiceDescriptionRepo, InvoiceMasterRepository $InvoiceMasterRepo, ELRRepository $ELRRepo)
    {
        $this->CityRepo = $CityRepo;
        $this->StatesRepo = $StatesRepo;
        $this->ZonesRepo = $ZonesRepo;
        $this->CreationRepo = $CreationRepo;
        $this->UserRepo = $UserRepo;
        $this->AffiliateRepo = $AffiliateRepo;
        $this->POCRepo = $POCRepo;
        $this->RequirementRepo = $RequirementRepo;
        $this->VehicleTypeRepo = $VehicleTypeRepo;
        $this->DeliveryPointsRepo = $DeliveryPointsRepo;
        $this->VendorMasterRepo = $VendorMasterRepo;
        $this->TripAssignRepo = $TripAssignRepo;
        $this->TripImgRepo = $TripImgRepo;
        $this->TripClosureRepo = $TripClosureRepo;
        $this->LocRepo = $LocRepo;
        $this->InvoiceDescriptionRepo = $InvoiceDescriptionRepo;
        $this->InvoiceMasterRepo = $InvoiceMasterRepo;
        $this->ELRRepo = $ELRRepo;
    }

            
    public function ViewTrip(Request $request)
    {  
    	$id = 1;
    	$ids = [];
    	$datas = $this->TripAssignRepo->GetByDriverId($id);
    	foreach ($datas as $key => $value) {
    		$ids[$key] = $value->trip_id;
    	}

        $data['trip'] =   $this->RequirementRepo->GetByTripIds($ids);
        $data['vendor'] =   $this->VendorMasterRepo->GetAllWithNem();
         return view('Driver.view-trip',compact('data'));  
    }


                
    public function upload_bills(Request $request)
    {  

	if($request->hasFile('ewayfill'))
    {   
        $eimage = $request->file('ewayfill');
        $EwayFile = time().'.'.$eimage->getClientOriginalExtension();
        $destinationPath = public_path('/images/Eway_Bill/');
        $eimage->move($destinationPath,$EwayFile); 
    }

	if($request->hasFile('lr'))
    {   
        $limage = $request->file('lr');
        $lrname = time().'.'.$limage->getClientOriginalExtension();
        $destinationPath = public_path('/images/LR/');
        $limage->move($destinationPath,$lrname); 
    }

	if($request->hasFile('client_invoice'))
    {   
        $ciimage = $request->file('client_invoice');
        $ciname = time().'.'.$ciimage->getClientOriginalExtension();
        $destinationPath = public_path('/images/InvoiceFile/');
        $ciimage->move($destinationPath,$ciname); 
    }
    	

        $in_data = [
            'client_invoice_img' => $ciname, 
            'updated_at' => Carbon::now()->toDateTimeString(),
            'updated_by' => 2
            ];

        
        $data1 = $this->InvoiceMasterRepo->UpdateByTrip($request->input('trip_id'), $in_data);	
    	
        $tripdata = [
            'ewaybill_file' => $EwayFile,
            'lr_file' =>  $lrname,
            'updated_at' => Carbon::now()->toDateTimeString(),
            'updated_by' => 2
            ];

        $data = $this->TripClosureRepo->UpdateByTrip($request->input('trip_id'), $tripdata);
    	

        if ($tripdata && $data1) 
        {
        	return back()->with('success', 'File upload successfully');
        }
        else
        {
			return back()->with('warning', 'File Not upload');
        }
    }

    





                
    public function trip_closure_update(Request $request)
    {  

	if($request->hasFile('pod'))
    {   
        $eimage = $request->file('pod');
        $EwayFile = time().'.'.$eimage->getClientOriginalExtension();
        $destinationPath = public_path('/images/Eway_Bill/');
        $eimage->move($destinationPath,$EwayFile); 
    }

	if($request->hasFile('lr'))
    {   
        $limage = $request->file('lr');
        $lrname = time().'.'.$limage->getClientOriginalExtension();
        $destinationPath = public_path('/images/LR/');
        $limage->move($destinationPath,$lrname); 
    }
    else
    {
    	$lrname='';
    }

	if($request->hasFile('fine_file'))
    {   
        $ciimage = $request->file('fine_file');
        $ciname = time().'.'.$ciimage->getClientOriginalExtension();
        $destinationPath = public_path('/images/InvoiceFile/');
        $ciimage->move($destinationPath,$ciname); 
    }
    else
    {
    	$ciname='';
    }
    	

    	
        $tripdata = [
            'count_of_shipment' =>  $request->input('shipment'),
            'POD_file' => $EwayFile,
            'damages' =>  $request->input('damages'),
            'damages_file' =>  $lrname,
            'fine' =>  $request->input('fine'),
            'fine_file' =>  $ciname,
            'updated_by' =>  1,
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];

        $data = $this->TripClosureRepo->UpdateByTrip($request->input('trip_id'), $tripdata);
    	

        if ($data) 
        {
        	return back()->with('success', 'File upload successfully');
        }
        else
        {
			return back()->with('warning', 'File Not upload');
        }
 
    }

    

            
            
    public function ViewTripInfo()
    {  
        $id = 1;
        $ids = [];
        $datas = $this->TripAssignRepo->GetByDriverId($id);
        foreach ($datas as $key => $value) {
            $ids[$key] = $value->trip_id;
        }

        $data['trip'] =   $this->RequirementRepo->GetByTripIds($ids);
        $data['vendor'] =   $this->VendorMasterRepo->GetAllWithNem();
         return view('Driver.view-trip-info',compact('data'));  
    }


    
    public function trip_view_info($id)
    { 
        $data['ELR'] = $this->ELRRepo->GetByTId($id);
        $data['Requirement'] = $this->RequirementRepo->GetById($id);
        $data['driver'] = DB::table('driver_master')->get();
        $data['Vehicle'] = DB::table('vehicle_master')->get();   
        $data['TripAssign'] = $this->TripAssignRepo->GetByTId($data['Requirement']->id);
        $data['States'] = $this->StatesRepo->GetById($data['Requirement']->p_state);
        $data['City'] = $this->CityRepo->GetById($data['Requirement']->p_city);
        $data['DP'] = $this->DeliveryPointsRepo->GetByTId($data['Requirement']->id);
        $data['TripClosure'] = $this->TripClosureRepo->GetByTId($data['Requirement']->id);
     
        if(isset($data['Requirement']->truck_type))
        {
            $data['VehicleType'] = $this->VehicleTypeRepo->GetById($data['Requirement']->truck_type);
        }
        else
        {
            $data['VehicleType'] = '';
        }
        return view('Driver.trip-assignments-form',compact('data'));
    }





}
