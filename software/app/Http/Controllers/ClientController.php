<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Location\City\CityRepository;
use App\Repositories\Location\States\StatesRepository;
use App\Repositories\Location\Zones\ZonesRepository;
use App\Repositories\Client\Creation\CreationRepository;
use App\Repositories\Client\Requirement\RequirementRepository;
use App\Repositories\Client\DeliveryPoints\DeliveryPointsRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\VehicleType\VehicleTypeRepository;
use App\Repositories\Affiliate\AffiliateRepository;
use App\Repositories\POC\POCRepository;
use App\Repositories\TripAssign\TripAssignRepository;
use Hash;
use Carbon\Carbon;


class ClientController extends Controller
{
	private $CityRepo;
	private $StatesRepo;
	private $ZonesRepo;
    private $CreationRepo;
    private $UserRepo;
    private $AffiliateRepo;
    private $POCRepo;
    private $RequirementRepo;
    private $VehicleTypeRepo;
    private $DeliveryPointsRepo;
    private $TripAssignRepo;
    
    public function __construct(CityRepository $CityRepo, StatesRepository $StatesRepo, ZonesRepository $ZonesRepo, CreationRepository $CreationRepo,
                                UserRepository $UserRepo, AffiliateRepository $AffiliateRepo, POCRepository $POCRepo,
                                RequirementRepository $RequirementRepo, VehicleTypeRepository $VehicleTypeRepo, DeliveryPointsRepository $DeliveryPointsRepo,
                                TripAssignRepository $TripAssignRepo)
    {
        $this->CityRepo = $CityRepo;
        $this->StatesRepo = $StatesRepo;
        $this->ZonesRepo = $ZonesRepo;
        $this->CreationRepo = $CreationRepo;
        $this->UserRepo = $UserRepo;
        $this->AffiliateRepo = $AffiliateRepo;
        $this->POCRepo = $POCRepo;
        $this->RequirementRepo = $RequirementRepo;
        $this->VehicleTypeRepo = $VehicleTypeRepo;
        $this->DeliveryPointsRepo = $DeliveryPointsRepo;
        $this->TripAssignRepo = $TripAssignRepo;
    }

    
    public function client_creation()
    {
		 return view('Client.client-creation');
    } 

    public function ClientCreation(Request $request)
    {
    	$this->validate($request, [
            'enterprise_name'=> 'required|max:191',
            'poc'=> 'required|max:191',
            'address'=> 'required|max:500',
            'organization_type'=> 'required',
            'delivery_type'=> 'required',
            'frequency'=> 'required',
            'gst'=> 'required',
            'mobile_number'=> 'required|numeric|digits:10|regex:/^[6-9]\d{9}$/|unique:users,mobile_no',
            'email'=> 'required|email|unique:users,email',
        ]);

        $user = [
            'name' => $request->input('enterprise_name'),
            'address' => $request->input('address'),
            'password' => Hash::make($request->input('mobile_number')),
            'menuroles' => 'user',
            'mobile_no' => $request->input('mobile_number'),
            'email' => $request->input('email'),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];

        $data1 =  $this->UserRepo->Create($user);

        $UserData =  $this->UserRepo->GetByMobile($request->input('mobile_number'));

        $client = [
            'user_id' => $UserData->id,
            'enterprise_name' => $request->input('enterprise_name'),
            'poc' => $request->input('poc'),
            'address' => $request->input('address'),
            'organization_type' => $request->input('organization_type'),
            'delivery_type' => $request->input('delivery_type'),
            'frequency' => $request->input('frequency'),
            'mobile_number' => $request->input('mobile_number'),
            'email' => $request->input('email'),
            'gst' => $request->input('gst')
                        ];
            
        $data2 =  $this->CreationRepo->Create($client);
        
        $affiliat = [
            'user_id' => $UserData->id,
            'mobile_no' => $request->input('mobile_number'),
            'company_id' => 1,
            'login_type' => 'user',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            'email' => $request->input('email')
            ];

        $data3 =  $this->AffiliateRepo->Create($affiliat);
    
        $poc = [
            'user_id' => $UserData->id,
            'name' =>$request->input('enterprise_name'),
            'mobile_number' => $request->input('mobile_number'),
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];

        $data4 =  $this->POCRepo->Create($poc);

        if($data1 && $data2 && $data3 && $data4)
        {
            return back()->with('success', 'Data Insert Successfully');
        }
        else
        {
            return back()->with('warning', 'Data Not Insert');
        }
    }


    public function view_clients()
    {
        $data['clients'] = $this->CreationRepo->GetAll();
		return view('Client.view-clients',compact('data'));
    }
    
    
    
    public function view_client_info($id)
    {
        $data['clients'] = $this->CreationRepo->GetById($id);
		return view('Client.view-client-creation',compact('data'));
    }



    public function edit_client_info($id)
    {
        $data['clients'] = $this->CreationRepo->GetById($id);
		return view('Client.edit-client-creation',compact('data'));
    }


    public function UpdateClientCreation(Request $request)
    {
    	$this->validate($request, [
            'enterprise_name'=> 'required|max:191',
            'poc'=> 'required|max:191',
            'address'=> 'required|max:500',
            'organization_type'=> 'required',
            'delivery_type'=> 'required',
            'frequency'=> 'required',
            'mobile_number'=> 'required|numeric|digits:10|regex:/^[6-9]\d{9}$/',
            'email'=> 'required|email',
            'gst'=> 'required',
        ]);
        
        $clientId = $request->input('id');
        $client = [
            'enterprise_name' => $request->input('enterprise_name'),
            'poc' => $request->input('poc'),
            'address' => $request->input('address'),
            'organization_type' => $request->input('organization_type'),
            'delivery_type' => $request->input('delivery_type'),
            'frequency' => $request->input('frequency'),
            'gst' => $request->input('gst')
                        ];


        $data =  $this->CreationRepo->Update($clientId, $client);


        if($data)
        {
            return redirect('view-clients')->with('success', 'Update Successfully');
        }
        else
        {
            return redirect('view-clients')->with('warning', 'Not Update');
        }
    }






    public function client_requirement()
    {
		
        $data['States'] = $this->StatesRepo->GetAll();
        $data['VehicleType'] = $this->VehicleTypeRepo->GetAll();
		return view('Client.client-requirement-form',compact('data'));
    }

    
    public function ClientRequirement(Request $request)
    {
         $this->validate($request, [
            'pickup_location_id'=> 'required',
            'pickup_location'=> 'required',
            'pickup_address'=> 'required|max:191',
            'pickup_state'=> 'required',
            'pickup_city'=> 'required',
            'pickup_landmark'=> 'required|max:191',
            'pickup_contact_person'=> 'required|max:191',
            'pickup_mob_no'=> 'required|numeric|digits:10', //|regex:^[6-9]\d{9}$

            'drop_location_id'=> 'required',
            'drop_location'=> 'required',
            'drop_address'=> 'required|max:191',
            'drop_state'=> 'required',
            'drop_city'=> 'required',
            'drop_landmark'=> 'required|max:191',
            'drop_contact_person'=> 'required|max:191',
            'drop_mob_no'=> 'required|numeric|digits:10', //|regex:^[6-9]\d{9}$


            'freight_total_weight'=> 'required|numeric',
            'freight_truck_type'=> 'required',
            'freight_shipment_type'=> 'required',
            'freight_type'=> 'required',
            'material_description'=> 'required|max:191',
            'gps'=> 'required',
            'loading_unloading'=> 'required',
            'pickup_date'=> 'required',
            'pickup_time'=> 'required',
            'dropup_date'=> 'required',
            'dropup_time'=> 'required',
            'additional_comments'=> 'nullable|max:191',

        ]); 
               $trip_id = 'TRIP'.rand(100000,999999);

               $Requirement = [
                        'trip_id' => $trip_id,
                        'client_id' => 1,
                        'p_location_id' => $request->input('pickup_location_id'),
                        'p_location' => $request->input('pickup_location'),
                        'p_address' => $request->input('pickup_address'),
                        'p_state' => $request->input('pickup_state'),
                        'p_state' => $request->input('pickup_state'),
                        'p_city' => $request->input('pickup_city'),
                        'p_landmark' => $request->input('pickup_landmark'),
                        'p_contact_person' => $request->input('pickup_contact_person'),
                        'p_mob_no' => $request->input('pickup_mob_no'),
                        'p_date' => $request->input('pickup_date'),
                        'p_time' => $request->input('pickup_time'),


                        'd_location_id' => $request->input('drop_location_id'),
                        'd_location' => $request->input('drop_location'),
                        'd_address' => $request->input('drop_address'),
                        'd_state' => $request->input('drop_state'),
                        'd_delivery_points' => $request->drop_delivery_points[0],
                        'd_city' => $request->input('drop_city'),
                        'd_landmark' => $request->input('drop_landmark'),
                        'd_contact_person' => $request->input('drop_contact_person'),
                        'd_mob_no' => $request->input('drop_mob_no'),
                        'd_date' => $request->input('dropup_date'),
                        'd_time' => $request->input('dropup_time'),

                        'total_weight' => $request->input('freight_total_weight'),
                        'truck_type' => $request->input('freight_truck_type'),
                        'shipment_type' => $request->input('freight_shipment_type'),
                        'freight_type' => $request->input('freight_type'),
                        'material_desc' => $request->input('material_description'),
                        'gps' => $request->input('gps'),
                        'created_by' => 1,
                        'updated_by' => 1,
                        'loading_unloading' => $request->input('loading_unloading'),
                        'comments' => $request->input('additional_comments'),
                        'trip_status' => 2
                        ];
                   

                    $data1 =   $this->RequirementRepo->Create($Requirement);
                

                    $id =  $this->RequirementRepo->GetByTrip($trip_id);

                    foreach($request->drop_delivery_points as $value)
                    {
                        $Point = [
                            'client_id' => 1, 
                            'trip_id' =>  $id->id,
                            'delivery_point' =>  $value,
                            'status' => 1,
                            ];

                        $data2 =    $this->DeliveryPointsRepo->Create($Point);
                    }
                 
                    
            session()->put('TripId', $trip_id);
            if($data1)
            {
                return redirect('trip-id')->with('success', 'Insert Successfully');
            }
            else
            {
                return back()->with('warning', 'Not Insert');
            }        
    }




    public function view_client_requirement()
    {
		
        $data['States'] = $this->StatesRepo->GetAll();
        $data['City'] = $this->CityRepo->GetAll();
        $data['VehicleType'] = $this->VehicleTypeRepo->GetAll();
        $data['Requirement'] = $this->RequirementRepo->GetAll();

		return view('Client.view-client-requirement',compact('data'));
    }



    public function edit_client_requirement($id)
    {

        $data['TripAssign'] = $this->TripAssignRepo->GetByTId($id);
        
        if($data['TripAssign'])
        {
            return redirect('view-clients-requirement')->with('warning', 'Cannot edit');
        }

        $data['States'] = $this->StatesRepo->GetAll();
        $data['City'] = $this->CityRepo->GetAll();
        $data['VehicleType'] = $this->VehicleTypeRepo->GetAll();
        $data['Requirement'] = $this->RequirementRepo->GetById($id);

        $data['DP'] = $this->DeliveryPointsRepo->GetByTId($data['Requirement']->id);

        if(isset($data['Requirement']->p_state))
        {
            $data['PCity'] = $this->CityRepo->GetByState($data['Requirement']->p_state);
        }
        else
        {
            $data['PCity'] = '';
        }
        if(isset($data['Requirement']->d_state))
        {
            $data['DCity'] = $this->CityRepo->GetByState($data['Requirement']->d_state);
        }
        else
        {
            $data['DCity'] = '';
        }
        
        
		return view('Client.edit-client-requirement-form',compact('data'));
    }



    
    public function update_client_requirement(Request $request)
    {
        $this->validate($request, [
            'pickup_location_id'=> 'required',
            'pickup_location'=> 'required',
            'pickup_address'=> 'required|max:191',
            'pickup_state'=> 'required',
            'pickup_city'=> 'required',
            'pickup_landmark'=> 'required|max:191',
            'pickup_contact_person'=> 'required|max:191',
            'pickup_mob_no'=> 'required|numeric|digits:10',

            'drop_location_id'=> 'required',
            'drop_location'=> 'required',
            'drop_address'=> 'required|max:191',
            'drop_state'=> 'required',
            'drop_delivery_points'=> 'required|max:191',
            'drop_city'=> 'required',
            'drop_landmark'=> 'required|max:191',
            'drop_contact_person'=> 'required|max:191',
            'drop_mob_no'=> 'required|numeric|digits:10',


            'freight_total_weight'=> 'required|numeric',
            'freight_truck_type'=> 'required',
            'freight_shipment_type'=> 'required',
            'freight_type'=> 'required',
            'material_description'=> 'required|max:191',
            'gps'=> 'required',
            'loading_unloading'=> 'required',
            'pickup_date'=> 'required',
            'pickup_time'=> 'required',
            'dropup_date'=> 'required',
            'dropup_time'=> 'required',
            'additional_comments'=> 'nullable|max:191',

        ]);
               $id = $request->input('id');
               $Requirement = [
                        'client_id' => 1,
                        'p_location_id' => $request->input('pickup_location_id'),
                        'p_location' => $request->input('pickup_location'),
                        'p_address' => $request->input('pickup_address'),
                        'p_state' => $request->input('pickup_state'),
                        'p_state' => $request->input('pickup_state'),
                        'p_city' => $request->input('pickup_city'),
                        'p_landmark' => $request->input('pickup_landmark'),
                        'p_contact_person' => $request->input('pickup_contact_person'),
                        'p_mob_no' => $request->input('pickup_mob_no'),
                        'p_date' => $request->input('pickup_date'),
                        'p_time' => $request->input('pickup_time'),

                        'd_location_id' => $request->input('drop_location_id'),
                        'd_location' => $request->input('drop_location'),
                        'd_address' => $request->input('drop_address'),
                        'd_state' => $request->input('drop_state'),
                        'd_delivery_points' => $request->drop_delivery_points[0],
                        'd_city' => $request->input('drop_city'),
                        'd_landmark' => $request->input('drop_landmark'),
                        'd_contact_person' => $request->input('drop_contact_person'),
                        'd_mob_no' => $request->input('drop_mob_no'),
                        'd_date' => $request->input('dropup_date'),
                        'd_time' => $request->input('dropup_time'),

                        'total_weight' => $request->input('freight_total_weight'),
                        'truck_type' => $request->input('freight_truck_type'),
                        'shipment_type' => $request->input('freight_shipment_type'),
                        'freight_type' => $request->input('freight_type'),
                        'material_desc' => $request->input('material_description'),
                        'gps' => $request->input('gps'),
                        'updated_by' => 1,
                        'loading_unloading' => $request->input('loading_unloading'),
                        'comments' => $request->input('additional_comments'),
                        'updated_at' => Carbon::now()->toDateTimeString()
                        ];

                        $data1 =   $this->RequirementRepo->Update($id, $Requirement);
                        $count = count($request->drop_delivery_id);
                     $number=0; 
                    
            foreach($request->drop_delivery_points as $key => $value)
            {
                if($count > 0)
                {
                    $Point = [
                        'client_id' => 1, 
                        'trip_id' =>  $id,
                        'delivery_point' =>  $value,
                        'status' => 1,
                        ];
                    
                    $data2 = $this->DeliveryPointsRepo->Update($request->drop_delivery_id[$key], $Point);
                    --$count;
                }
                else{

                    $Point = [
                        'client_id' => 1, 
                        'trip_id' =>  $id,
                        'delivery_point' =>  $value,
                        'status' => 1,
                        ];

                    $data2 =    $this->DeliveryPointsRepo->Create($Point);
                }
               
            }
         
            if($data1 )
            {
                return redirect('view-clients-requirement')->with('success', 'Update Successfully');
            }
            else
            {
                return redirect('view-clients-requirement')->with('warning', 'Not Update');
            }        
    }












}
