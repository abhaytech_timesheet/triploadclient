<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Location\City\CityRepository;
use App\Repositories\Location\States\StatesRepository;
use App\Repositories\Location\Zones\ZonesRepository;
use App\Repositories\Client\Creation\CreationRepository;
use App\Repositories\Client\Requirement\RequirementRepository;
use App\Repositories\Client\DeliveryPoints\DeliveryPointsRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\VehicleType\VehicleTypeRepository;
use App\Repositories\Affiliate\AffiliateRepository;
use App\Repositories\POC\POCRepository;
use App\Repositories\POC\TripRepository;
use App\Repositories\Vendor\VendorMaster\VendorMasterRepository;
use App\Repositories\TripAssign\TripAssignRepository;
use App\Repositories\Location\Loc\LocRepository;
use App\Repositories\TripImg\TripImgRepository;
use App\Repositories\TripClosure\TripClosureRepository;
use App\Repositories\ELR\ELRRepository;
use App\Repositories\Invoice\InvoiceDescription\InvoiceDescriptionRepository;
use App\Repositories\Invoice\InvoiceMaster\InvoiceMasterRepository;
use Hash;
use DB;
use PDF;
use Image;
use Carbon\Carbon;


class TripController extends Controller
{
	private $CityRepo;
	private $StatesRepo;
	private $ZonesRepo;
    private $CreationRepo;
    private $UserRepo;
    private $AffiliateRepo;
    private $POCRepo;
    private $RequirementRepo;
    private $VehicleTypeRepo;
    private $DeliveryPointsRepo;
    private $VendorMasterRepo;
    private $TripAssignRepo;
    private $TripImgRepo;
    private $TripClosureRepo;
    private $LocRepo;
    private $InvoiceDescriptionRepo;
    private $InvoiceMasterRepo;
    private $ELRRepo;
    
    public function __construct(CityRepository $CityRepo, StatesRepository $StatesRepo, ZonesRepository $ZonesRepo, CreationRepository $CreationRepo, UserRepository $UserRepo, AffiliateRepository $AffiliateRepo, POCRepository $POCRepo, RequirementRepository $RequirementRepo, VehicleTypeRepository $VehicleTypeRepo, DeliveryPointsRepository $DeliveryPointsRepo,  VendorMasterRepository $VendorMasterRepo, TripAssignRepository $TripAssignRepo, TripImgRepository $TripImgRepo , TripClosureRepository $TripClosureRepo, LocRepository $LocRepo, InvoiceDescriptionRepository $InvoiceDescriptionRepo, InvoiceMasterRepository $InvoiceMasterRepo, ELRRepository $ELRRepo)
    {
        $this->CityRepo = $CityRepo;
        $this->StatesRepo = $StatesRepo;
        $this->ZonesRepo = $ZonesRepo;
        $this->CreationRepo = $CreationRepo;
        $this->UserRepo = $UserRepo;
        $this->AffiliateRepo = $AffiliateRepo;
        $this->POCRepo = $POCRepo;
        $this->RequirementRepo = $RequirementRepo;
        $this->VehicleTypeRepo = $VehicleTypeRepo;
        $this->DeliveryPointsRepo = $DeliveryPointsRepo;
        $this->VendorMasterRepo = $VendorMasterRepo;
        $this->TripAssignRepo = $TripAssignRepo;
        $this->TripImgRepo = $TripImgRepo;
        $this->TripClosureRepo = $TripClosureRepo;
        $this->LocRepo = $LocRepo;
        $this->InvoiceDescriptionRepo = $InvoiceDescriptionRepo;
        $this->InvoiceMasterRepo = $InvoiceMasterRepo;
        $this->ELRRepo = $ELRRepo;
    }

    
    public function create_trip()
    { 
        $data['vendor'] =   $this->VendorMasterRepo->GetAllWithNem();
        $data['trip'] =   $this->RequirementRepo->GetAll();
		return view('Trip.trip-creation',compact('data'));
    }

    public function CreateTrip(Request $request)
    {

    	$this->validate($request, [
            'trip_id'=> 'required',
            'vendor'=> 'required',
            'agreed_sales_value'=> 'required|numeric',
            'vendor_purchase_value'=> 'required|numeric',
        ]);

        $tripdata = [
            'alloted_to_vendor_id' => $request->input('vendor'),
            'agreed_sales_value' => $request->input('agreed_sales_value'),
            'vendor_purchase_value' => $request->input('vendor_purchase_value'),
            'trip_status' => 4,
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];
            
        
        $data =   $this->RequirementRepo->UpdateByTrip($request->input('trip_id'), $tripdata);

        if($data)
        {
            return back()->with('success', 'Update Successfully');
        }
        else
        {
            return back()->with('warning', 'Not Update');
        }
    }


            
    public function ViewTrip(Request $request)
    {  
        $data['trip'] =   $this->RequirementRepo->GetByCreateTrip();
        $data['vendor'] =   $this->VendorMasterRepo->GetAllWithNem();
         return view('Trip.view-trip',compact('data'));  
    }

    

    public function GetTrips(Request $request)
    { 
        $data['Requirement'] = $this->RequirementRepo->GetByTrip($request->id);
        if(empty($data['Requirement']))
        {
            return 'DATA NOT FOUND';
        }
        else
        {
            $d['asv'] = $data['Requirement']->agreed_sales_value;
            $d['vpv'] = $data['Requirement']->vendor_purchase_value;
            $d['atvi'] = $data['Requirement']->alloted_to_vendor_id;
            return $d;
        }		
    }


    
    public function trip_assignments($id)
    { 
        $data['ELR'] = $this->ELRRepo->GetByTId($id);
        $data['Requirement'] = $this->RequirementRepo->GetById($id);
        $data['driver'] = DB::table('driver_master')->get();
        $data['Vehicle'] = DB::table('vehicle_master')->get();   
        $data['TripAssign'] = $this->TripAssignRepo->GetByTId($data['Requirement']->id);
        $data['States'] = $this->StatesRepo->GetById($data['Requirement']->p_state);
        $data['City'] = $this->CityRepo->GetById($data['Requirement']->p_city);
        $data['DP'] = $this->DeliveryPointsRepo->GetByTId($data['Requirement']->id);

        if($data['TripAssign'])
        {
            $data['Aid'] = $data['TripAssign']->id;
            $data['DId'] = $data['TripAssign']->driver_id;
            $data['VTId'] = $data['TripAssign']->vehicle_type_id;
        }
        else
        {
            $data['Aid'] = '';
            $data['DId'] = '';
            $data['VTId'] = '';
        }
     
        if(isset($data['Requirement']->truck_type))
        {
            $data['VehicleType'] = $this->VehicleTypeRepo->GetById($data['Requirement']->truck_type);
        }
        else
        {
            $data['VehicleType'] = '';
        }

		return view('Trip.trip-assignments-form',compact('data'));
    }



    public function rejectTripAssign(Request $request)
    {
            $statu = [
                'trip_status' => 3,
                'updated_at' => Carbon::now()->toDateTimeString(),
                ];
                
            $data1 =   $this->RequirementRepo->Update($request->id , $statu);

            if($data1)
            {
                return 'update';
            }
            else
            {
                return 'error';
            }
    }



    
    public function CreateTripAssign(Request $request)
    {
    	$this->validate($request, [
            'trip_id'=> 'required',
            'vehicle_type'=> 'required',
            'driver'=> 'required',
            'date'=> 'required',
            'time'=> 'required',
        ]);

    if($request->input('id') == null)
    {

            $tripdata = [
                'client_id' => 1,
                'elr_num' =>  'ELR'.rand(100000,999999),
                'trip_id' => $request->input('trip_id'),
                'vehicle_type_id' => $request->input('vehicle_type'),
                'driver_id' => $request->input('driver'),
                'tentative_date' => $request->input('date'),
                'tentative_time' => $request->input('time'),
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
                ];     
                
            $data  =  $this->TripAssignRepo->Create($tripdata);
            
            $statu = [
                'trip_status' => 1,
                'updated_at' => Carbon::now()->toDateTimeString(),
                ];
                
            $data1 =   $this->RequirementRepo->Update($request->input('trip_id'), $statu);

            if($data && $data1)
            {
                return back()->with('success', 'Trip Accept Successfully');
            }
            else
            {
                return back()->with('warning', 'error');
            }
    }
    else
    {
        $tripdata = [
            'vehicle_type_id' => $request->input('vehicle_type'),
            'driver_id' => $request->input('driver'),
            'tentative_date' => $request->input('date'),
            'tentative_time' => $request->input('time'),
            'updated_by' => 2,
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];
            
        $data =   $this->TripAssignRepo->Update($request->input('id') ,$tripdata);

        if($data)
        {
            return back()->with('success', 'Update Successfully');
        }
        else
        {
            return back()->with('warning', 'Not Update');
        }
    }

    }




        public function View_Create_LR_File(Request $request,$id){
     
        $data['t'] = $this->RequirementRepo->GetById($id);
        $data['ELR'] = $this->ELRRepo->GetByTId($id);

        $data['pl']  = $this->LocRepo->GetByidandComp($data['t']->p_location_id);
        $data['dl']  = $this->LocRepo->GetByidandComp($data['t']->d_location_id);

        $data['TripAssign'] = $this->TripAssignRepo->GetByTId($id);
        $data['driver'] = DB::table('driver_master')->where('id', $data['TripAssign']->driver_id)->first();
        $data['States'] = $this->StatesRepo->GetAll();
        $data['City'] = $this->CityRepo->GetAll();
        $data['Vehicle'] = DB::table('vehicle_master')->where('id', $data['TripAssign']->vehicle_type_id)->first();  
        //dd($data['Vehicle'], $data['driver'],$data['t'],$data['TripAssign'],$data['pl'], $data['dl']);

        if(count($data['ELR']) > 0)
        {
            $data['ELR'] = $data['ELR'];
            return view('Trip.edit-eLR', compact('data'));
        }
        else
        {
            $data['ELR'] = '';
            return view('Trip.Create-eLR', compact('data'));
        }        
    }




    public function Create_LR_File(Request $request){
     
         $dd= $this->validate($request, [
            'invoice_no'=> 'required',
            'e_waybill_no'=> 'required',
            'box_count'=> 'required',
            'invoice_value'=> 'required',
            'trip_id'=> 'required',
        ]);

        $data['TripAssign'] = $this->TripAssignRepo->GetByTId($request->input('trip_id'));
        $assing_table_id = $data['TripAssign']->id; 

        $inv = $request->input('invoice_no');

        foreach ($inv as $key => $value) {

            $tripdata = [
                'trip_id' => $request->input('trip_id'),
                'trip_assign_id' => $assing_table_id,
                'invoice_no' =>  $request->invoice_no[$key],
                'e_waybill' => $request->e_waybill_no[$key],
                'box_count' => $request->box_count[$key],
                'invoice_val' => $request->invoice_value[$key],
                'created_by' => session()->get('id'),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
                ];

            $create = $this->ELRRepo->Create($tripdata);
        }

        if($create)
        {
            return redirect('trip-assignments/'.$request->input('trip_id'))->with('success','Create Successfully' );
        }
        else
        {
            return redirect('trip-assignments/'.$request->input('trip_id'))->with('warning','error' );
        }   
    }









    public function update_LR_File(Request $request){
     
         $dd= $this->validate($request, [
            'invoice_no'=> 'required',
            'e_waybill_no'=> 'required',
            'box_count'=> 'required',
            'invoice_value'=> 'required',
            'trip_id'=> 'required',
        ]);

        $data['TripAssign'] = $this->TripAssignRepo->GetByTId($request->input('trip_id'));
        $assing_table_id = $data['TripAssign']->id; 

        $inv = $request->input('invoice_no');
        $count = count($request->input('id'));
        $number=0; 

        foreach ($inv as $key => $value) {


                if($count > 0)
                {
                    $tripdata = [
                    'trip_id' => $request->input('trip_id'),
                    'trip_assign_id' => $assing_table_id,
                    'invoice_no' =>  $request->invoice_no[$key],
                    'e_waybill' => $request->e_waybill_no[$key],
                    'box_count' => $request->box_count[$key],
                    'invoice_val' => $request->invoice_value[$key],
                    'updated_by' => session()->get('id'),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    ];

                    $data =   $this->ELRRepo->Update($request->id[$key], $tripdata);
                    
                    --$count;
                }
                else{

                    $tripdata = [
                    'trip_id' => $request->input('trip_id'),
                    'trip_assign_id' => $assing_table_id,
                    'invoice_no' =>  $request->invoice_no[$key],
                    'e_waybill' => $request->e_waybill_no[$key],
                    'box_count' => $request->box_count[$key],
                    'invoice_val' => $request->invoice_value[$key],
                    'created_by' => session()->get('id'),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                    ];

                     $data =   $this->ELRRepo->Create($tripdata);
                }
        }


        if($data)
        {
            return redirect('trip-assignments/'.$request->input('trip_id'))->with('success','Update Successfully' );
        }
        else
        {
            return redirect('trip-assignments/'.$request->input('trip_id'))->with('warning','error' );
        }   
    }













    public function Download_LR_File(Request $request,$id){
     
        $data['t'] = $this->RequirementRepo->GetById($id);
        $data['pl']  = $this->LocRepo->GetByidandComp($data['t']->p_location_id);
        $data['dl']  = $this->LocRepo->GetByidandComp($data['t']->d_location_id);
        $data['ELR'] = $this->ELRRepo->GetByTId($id);
        $data['TripAssign'] = $this->TripAssignRepo->GetByTId($id);
        $data['driver'] = DB::table('driver_master')->where('id', $data['TripAssign']->driver_id)->first();
        $data['States'] = $this->StatesRepo->GetAll();
        $data['City'] = $this->CityRepo->GetAll();
        $data['Vehicle'] = DB::table('vehicle_master')->where('id', $data['TripAssign']->vehicle_type_id)->first();  
        //dd($data['Vehicle'], $data['driver'],$data['t'],$data['TripAssign'],$data['pl'], $data['dl']);
        return view('InvoiceFile.eLR', compact('data'));
        //$pdf = PDF::loadView('InvoiceFile.eLR', compact('data'))->setPaper('a4', 'landscape');
        //return $pdf->download('eLR_'.$data['t']->trip_id.'.pdf');          
    }

    

    public function Trip_Closure($id)
    { 
        $data['Requirement'] = $this->RequirementRepo->GetById($id);
        $data['TripAssign'] = $this->TripAssignRepo->GetByTId($id);
        $data['trip'] = $this->TripClosureRepo->GetByTId($id);
        if(!empty($data['trip']))
        {
            return back()->with('warning', 'Trip Already Closed');
        }
   
        if($data['TripAssign'] == null)
        {
            return back()->with('warning','First Assigned Trip After Close Trip');
        }
        $data['delivery'] = $this->DeliveryPointsRepo->GetByTId($id);
        $data['cos'] = count($data['delivery']);
        $data['PStates'] = $this->StatesRepo->GetById($data['Requirement']->p_state);
        $data['PCity'] = $this->CityRepo->GetById($data['Requirement']->p_city);
        $data['DStates'] = $this->StatesRepo->GetById($data['Requirement']->d_state);
        $data['DCity'] = $this->CityRepo->GetById($data['Requirement']->d_city);

        $data['Vehicle'] = DB::table('vehicle_master')->where('id', $data['TripAssign']->vehicle_type_id)->first();  

        return view('Trip.partial-trip-closure',compact('data'));	
    }



    public function Trip_Closure_add(Request $request)
    { 
    	$this->validate($request, [
            'trip_id'=> 'required',
            'lr_number'=> 'required',
            'lr_file'=> 'required|image|mimes:jpeg,png,jpg,pdf|max:20048',
            'vehicle_no'=> 'required',
            'lr_date'=> 'required',
            'eway_bill'=> 'required',
            'eway_bill_file'=> 'required|image|mimes:jpeg,png,jpg,pdf|max:20048',
            'from'=> 'required',
            'to'=> 'required',
            'count_of_shipment'=> 'required',
            'invoice_value'=> 'required',           
        ]);


        if($request->hasFile('lr_file'))
        {   
            $lrimage = $request->file('lr_file');
            $lrname = time().'.'.$lrimage->getClientOriginalExtension();
            $destinationPath = public_path('/images/LR/');
            $lrimage->move($destinationPath,$lrname); 
        }
        else
        {
            $lrimage = '';
        }

        if($request->hasFile('eway_bill_file'))
        {   
            $ebimage = $request->file('eway_bill_file');
            $ebname = time().'.'.$ebimage->getClientOriginalExtension();
            $destinationPath = public_path('/images/Eway_Bill/');
            $ebimage->move($destinationPath,$ebname); 
        }
        else
        {
            $ebimage = '';
        }
    
    if($request->input('OPD') != null)
    {
        $opd = $request->input('OPD');

        if($request->hasFile('POD_file'))
        {   
            $opdimage = $request->file('POD_file');
            $opdname = time().'.'.$opdimage->getClientOriginalExtension();
            $destinationPath = public_path('/images/OPD/');
            $opdimage->move($destinationPath,$opdname); 
        }
        else
        {
            $opdname = '';
        }
    }
    else
    {
        $opd= 'No';
    }

    if($request->hasFile('damages_images'))
    {
        foreach($request->file('damages_images') as $value)  
        {
        $image = $value;
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images/Damages/');
        $image->move($destinationPath,$name); 

        $imgdata = [
            'trip_id' => $request->input('trip_id'),
            'file_type' => 'Damages',
            'file' => $name,
            'created_by' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];

            $this->TripImgRepo->Create($imgdata);
        } 
    }

    if($request->hasFile('fine_image'))
    {   
        $fimage = $request->file('fine_image');
        $fname = time().'.'.$fimage->getClientOriginalExtension();
        $destinationPath = public_path('/images/Fine/');
        $fimage->move($destinationPath,$fname); 
    }

        $tripdata = [
            'trip_id' => $request->input('trip_id'), 
            'lr_number' =>  $request->input('lr_number'),
            'lr_file' =>  $lrname,
            'lr_date' =>  $request->input('lr_date'),
            'ewaybill_number' =>  $request->input('eway_bill'),
            'ewaybill_file' =>  $ebname,
            'from_address' => $request->input('from'),
            'to_address' => $request->input('to'),
            'vehicle_number' => $request->input('vehicle_no'),
            'count_of_shipment' => $request->input('count_of_shipment'),
            'invoice' => $request->input('invoice_value'),
            'POD' => $opd,
            'POD_file' => $opdname,
            'damages' => $request->input('damages'),
            'damages_file' => '',
            'fine' => $request->input('fine'),
            'fine_file' => $fname,
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];

           $data = $this->TripClosureRepo->Create($tripdata);




        $in_data = [
            'trip_id' => $request->input('trip_id'), 
            'invoice_no' =>  'ADV/'.date('d-y').'/0'.$request->input('trip_id'),
            'e_way_bill' =>  $request->input('eway_bill'),
            'place_of_supp' =>  $request->input('lr_date'),
            'lr_no' =>  $request->input('eway_bill'),
            'bill_to' =>  $request->input('to'),
            'due_date' => Carbon::now()->toDateTimeString(),
            'created_by' => 1,
            'status' => 1,
            'pod_received' => $opd,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];

        
        $data1 = $this->InvoiceMasterRepo->Create($in_data);


           if($data && $data1)
           {
               return back()->with('success', 'Trip Closure Successfully');
           }
           else
           {
               return back()->with('warning', 'Trip Not Closure ');
           }
       
    }


       

    public function ViewTripClosureReport()
    { 
        $data['trip_c'] = $this->TripClosureRepo->GetAll();
        
        $data['Requirement'] = $this->RequirementRepo->GetByCreateTrip();
        $data['vendor'] =   $this->VendorMasterRepo->GetAll();
        $data['user'] =   $this->UserRepo->GetAll();
        $data['TripAssign'] = $this->TripAssignRepo->GetAll();
        return view('Trip.view-trip-closure-report',compact('data'));	
    }


   
    public function view_invoice_report()
    {   
        $data['in'] = $this->InvoiceMasterRepo->GetAllWithTrip();


        return view('Invoice.view-Invoice',compact('data'));
    }


   
    public function find_invoice_report($id)
    {   
        if($id)
        {
            $data['in'] = $this->InvoiceMasterRepo->GetByIdWithTrip($id);

            if(count($data['in']) != 0)
            {
                return view('Invoice.view-Invoice',compact('data'));
            }
            else
            {
                return back()->with('warning','Recode Not Found!');
            }
        }
        else
        {
            return back()->with('warning','Recode Not Found!');
        }
    }

   
    public function generate_invoice($id)
    {
        if($id != null)
        {
            $data['in'] = $this->InvoiceMasterRepo->GetById($id);
            $data['Requir'] = $this->RequirementRepo->GetById($data['in']->trip_id);
            $data['TripAssign'] = $this->TripAssignRepo->GetByTId($data['in']->trip_id);

            return view('Invoice.trip-invoice-form',compact('data'));
        }
        else
        {
            return back()->with('warning', 'Recode Not found');
        }
    }




    public function create_generate_invoice(Request $request)
    {
        $this->validate($request, [
            'in_id'=> 'required',
            'description'=> 'required',
            'amount'=> 'required',
        ]);



    foreach ($request->input('description') as $key => $value) {

        $tripdata = [
            'invoice_id' => $request->input('in_id'),
            'p_description' => $request->description[$key],
            'amount' => $request->amount[$key],
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
            ];

             $data =   $this->InvoiceDescriptionRepo->Create($tripdata);
    }
   
        if($data)
        {
            return redirect('invoice-file/'.$request->input('in_id'))->with('success', 'Update Successfully');
        }
        else
        {
            return back()->with('warning', 'Not Update');
        }
    }


    public function view_invoice_file($id)
    {
        if($id != null)
        {
            $data['in'] = $this->InvoiceMasterRepo->GetById($id);
            $data['Requir'] = $this->RequirementRepo->GetById($data['in']->trip_id);
            $data['TripAssign'] = $this->TripAssignRepo->GetByTId($data['in']->trip_id);
            $data['InDesc'] =   $this->InvoiceDescriptionRepo->GetByInId($data['in']->id);

            if(!empty($data['InDesc']))
            {
                $data['InDesc'] = $data['InDesc'];
            }
            else
            {
                return back()->with('warning', 'Please Generate Invoice');
            }

            return view('Invoice.trip-invoice-repo',compact('data'));
        }
        else
        {
            return back()->with('warning', 'Recode Not found');
        }
    }


    public function Download_Invoice_File($id){
     
        if($id != null)
        {
            $data['in'] = $this->InvoiceMasterRepo->GetById($id);
            $data['Requir'] = $this->RequirementRepo->GetById($data['in']->trip_id);
            $data['TripAssign'] = $this->TripAssignRepo->GetByTId($data['in']->trip_id);
            $data['InDesc'] =   $this->InvoiceDescriptionRepo->GetByInId($data['in']->id);

            if(!empty($data['InDesc']))
            {
                $data['InDesc'] = $data['InDesc'];
            }
            else
            {
                return back()->with('warning', 'Please Generate Invoice');
            }

                //dd($data['Vehicle'], $data['driver'],$data['t'],$data['TripAssign'],$data['pl'], $data['dl']);
                //return view('InvoiceFile.eLR', compact('data'));
                $pdf = PDF::loadView('InvoiceFile.trip-invoice', compact('data'))->setWarnings(false);
                return $pdf->download($data['in']->invoice_no.'_Invoice.pdf');  
        }
        else
        {
            return back()->with('warning', 'Recode Not found');
        } 
        
    }


    public function invoice_update($id, $value)
    {

        if($id != null && $value != null)
        {
            $data = $this->InvoiceMasterRepo->Update($id, ['approve_status' => $value]);

            if($data)
            {
                 return back()->with('success', 'States Change Successfully');
            }
            else
            {
                 return back()->with('warning', 'Recode Not found');
            }

        }
    }
    
    


   
    public function edit_generate_invoice($id)
    {
        if($id != null)
        {
            $data['in'] = $this->InvoiceMasterRepo->GetById($id);
            $data['Requir'] = $this->RequirementRepo->GetById($data['in']->trip_id);
            $data['TripAssign'] = $this->TripAssignRepo->GetByTId($data['in']->trip_id);
            $data['InDesc'] =   $this->InvoiceDescriptionRepo->GetByInId($data['in']->id);
            if(!empty($data['InDesc']))
            {
                $data['InDesc'] = $data['InDesc'];
            }
            else
            {
                return back()->with('warning', 'Please Generate Invoice');
            }

            return view('Invoice.edit-trip-invoice-form',compact('data'));
        }
        else
        {
            return back()->with('warning', 'Recode Not found');
        }
    }




    public function update_generate_invoice(Request $request)
    {
        $this->validate($request, [
            'in_id'=> 'required',
            'description'=> 'required',
            'amount'=> 'required',
        ]);


            $count = count($request->desc_id);
            $number=0; 
                    
            foreach($request->description as $key => $value)
            {
                if($count > 0)
                {
                    $tripdata = [
                        'p_description' => $request->description[$key],
                        'amount' => $request->amount[$key],
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ];

                     $data =   $this->InvoiceDescriptionRepo->Update($request->desc_id[$key], $tripdata);
                    
                    --$count;
                }
                else{

                    $tripdata = [
                        'invoice_id' => $request->input('in_id'),
                        'p_description' => $request->description[$key],
                        'amount' => $request->amount[$key],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ];

                     $data =   $this->InvoiceDescriptionRepo->Create($tripdata);
                }
               
            }

   
        if($data)
        {
            return redirect('invoice-file/'.$request->input('in_id'))->with('success', 'Update Successfully');
        }
        else
        {
            return back()->with('warning', 'Not Update');
        }
    }













}
