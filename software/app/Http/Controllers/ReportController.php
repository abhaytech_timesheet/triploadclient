<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Location\City\CityRepository;
use App\Repositories\Location\States\StatesRepository;
use App\Repositories\Location\Zones\ZonesRepository;
use App\Repositories\Client\Creation\CreationRepository;
use App\Repositories\Client\Requirement\RequirementRepository;
use App\Repositories\Client\DeliveryPoints\DeliveryPointsRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\VehicleType\VehicleTypeRepository;
use App\Repositories\Affiliate\AffiliateRepository;
use App\Repositories\POC\POCRepository;
use App\Repositories\POC\TripRepository;
use App\Repositories\Vendor\VendorMaster\VendorMasterRepository;
use App\Repositories\TripAssign\TripAssignRepository;
use App\Repositories\Location\Loc\LocRepository;

use App\Repositories\TripImg\TripImgRepository;
use App\Repositories\TripClosure\TripClosureRepository;

use Hash;
use DB;
use PDF;
use Image;
use Carbon\Carbon;

class ReportController extends Controller
{
   	private $CityRepo;
	private $StatesRepo;
	private $ZonesRepo;
    private $CreationRepo;
    private $UserRepo;
    private $AffiliateRepo;
    private $POCRepo;
    private $RequirementRepo;
    private $VehicleTypeRepo;
    private $DeliveryPointsRepo;
    private $VendorMasterRepo;
    private $TripAssignRepo;
    private $TripImgRepo;
    private $TripClosureRepo;
    private $LocRepo;
    
    public function __construct(CityRepository $CityRepo, StatesRepository $StatesRepo, ZonesRepository $ZonesRepo, CreationRepository $CreationRepo,
                                UserRepository $UserRepo, AffiliateRepository $AffiliateRepo, POCRepository $POCRepo,
                                RequirementRepository $RequirementRepo, VehicleTypeRepository $VehicleTypeRepo,
                                 DeliveryPointsRepository $DeliveryPointsRepo,  VendorMasterRepository $VendorMasterRepo,
                                  TripAssignRepository $TripAssignRepo, TripImgRepository $TripImgRepo , TripClosureRepository $TripClosureRepo,
                                  LocRepository $LocRepo)
    {
        $this->CityRepo = $CityRepo;
        $this->StatesRepo = $StatesRepo;
        $this->ZonesRepo = $ZonesRepo;
        $this->CreationRepo = $CreationRepo;
        $this->UserRepo = $UserRepo;
        $this->AffiliateRepo = $AffiliateRepo;
        $this->POCRepo = $POCRepo;
        $this->RequirementRepo = $RequirementRepo;
        $this->VehicleTypeRepo = $VehicleTypeRepo;
        $this->DeliveryPointsRepo = $DeliveryPointsRepo;
        $this->VendorMasterRepo = $VendorMasterRepo;
        $this->TripAssignRepo = $TripAssignRepo;
        $this->TripImgRepo = $TripImgRepo;
        $this->TripClosureRepo = $TripClosureRepo;
        $this->LocRepo = $LocRepo;
    }


    public function b_opportunity_report()
    {
    	return view('Report.b_opportunity_report');
    }

    public function trip_report()
    {
    	return view('Report.trip_report');
    }

    public function FinanceReport()
    {
    	return view('Report.finance_report');
    }

}
