<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Repositories\Location\City\CityRepository;
use App\Repositories\Location\States\StatesRepository;
use App\Repositories\Location\Zones\ZonesRepository;
use App\Repositories\Location\Loc\LocRepository;

class LocationController extends Controller
{

	private $CityRepo;
	private $StatesRepo;
	private $ZonesRepo;
	private $LocRepo;

	public function __construct(CityRepository $CityRepo, StatesRepository $StatesRepo, ZonesRepository $ZonesRepo, LocRepository $LocRepo)
    {
        $this->CityRepo = $CityRepo;
        $this->StatesRepo = $StatesRepo;
		$this->ZonesRepo = $ZonesRepo;
		$this->LocRepo = $LocRepo;
    }

    public function get_city(Request $Request)
    {
    	if($Request->id != null)
    	{
    		$data['City'] = $this->CityRepo->GetByState($Request->id);

    		if(!empty($data['City']))
    		{
    			$City = '';

    			$City .= '<option value="">SELECT CITY</option>';

    			foreach ($data['City'] as $value) {
    				$City .= '<option value="'.$value->id.'">'.$value->name.'</option>';
    			}

    			return $City;
    		}
    		else
    		{
    			return 'DATA NOT FOUND';
    		}
    	}
    	else
    	{
    		return 'DATA NOT FOUND';
    	}
	}
	


	
    public function get_location(Request $Request)
    {
    	if($Request->name != null)
    	{
    		$data['loc'] = $this->LocRepo->SearchByName($Request->name);
		
    		if(!empty($data['loc']))
    		{
    			return $data['loc'];
    		}
    		else
    		{
    			return 'DATA NOT FOUND';
    		}
    	}
    	else
    	{
    		return 'DATA NOT FOUND';
    	}
	}
	


}
