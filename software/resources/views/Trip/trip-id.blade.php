@extends('mainlayout')
@section('content')
            <main class="c-main">
                <div class="container-fluid">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
                    <div class="fade-in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header text-center">
                                        <span><i class="c-icon c-icon-4xl mb-2 cil-check"></i><br>
                                           Trip Id :<strong>@if(session()->get('TripId')) {{session()->get('TripId')}} @endif</strong><br>
                                           Our Sales team shall reach you with the best quote.
                                        </span>
                                    </div>
                                    <div class="card-footer">
                                        <a class="btn btn-sm btn-danger" href="{{url('client-requirement-form')}}"> Back</a>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                        </div>
                        <!-- /.row-->
                    </div>
                </div>
            </main>
@endsection