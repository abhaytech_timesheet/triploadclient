@extends('mainlayout')
@section('content')
            <main class="c-main">
                <div class="container-fluid">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
                    <div class="fade-in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Trip Assignments Form</a></li>
                                        </ol>
                                    </nav>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal" action="{{url('trip-assignment')}}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_trip_id">Trip ID</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_trip_id" type="text" name="txt_trip_id" placeholder="Autofill" value="{{$data['Requirement']->trip_id}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_from">From</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_from" type="text" name="txt_from" placeholder="Autofill" value="{{$data['Requirement']->p_address}} {{$data['Requirement']->p_landmark}} {{ $data['States']->name}} {{$data['City']->name}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_type_of_load">Type Of Load</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_type_of_load" type="text" name="txt_type_of_load" placeholder="Autofill"  value="{{$data['Requirement']->freight_type}}" readonly> 
                                                </div>
                                            </div>
                                            @php $sn=0; @endphp
                                            @foreach($data['DP'] as $value)
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_points_of_del">Points of Del. {{++$sn}} </label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_points_of_del" type="text" name="txt_points_of_del" placeholder="Autofill" value="{{$value->delivery_point}}" readonly>
                                                </div>
                                            </div>
                                            @endforeach 
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_weight">Weight </label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_weight" type="text" name="txt_weight" placeholder="Autofill" value="{{$data['Requirement']->total_weight}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_truck_type">Truck Type </label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_truck_type" type="text" name="txt_truck_type" placeholder="Autofill" value="{{$data['VehicleType']->vehicle_type}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_type_of_shipment">Type of Shipment </label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_type_of_shipment" type="text" name="txt_type_of_shipment" placeholder="Autofill" value="{{$data['Requirement']->shipment_type}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_description">Material Description</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_description" type="text" name="txt_description" placeholder="Autofill" value="{{$data['Requirement']->material_desc}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_gps">GPS</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_gps" type="text" name="txt_gps" placeholder="Autofill" value="{{$data['Requirement']->gps}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_load_unload">Loading/Unloading</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_load_unload" type="text" name="txt_load_unload" placeholder="Autofill" value="{{$data['Requirement']->loading_unloading}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_pickup_time">Pickup Date and Time</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_pickup_time" type="text" name="txt_pickup_time" placeholder="Autofill" value="{{$data['Requirement']->p_date}} {{$data['Requirement']->p_time}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_dropup_time">Drop Date and Time</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_dropup_time" type="text" name="txt_dropup_time" placeholder="Autofill" value="{{$data['Requirement']->d_date}} {{$data['Requirement']->d_time}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_comments">Additional Comments</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="txt_comments" type="text" name="txt_comments" placeholder="Autofill" value="{{$data['Requirement']->comments}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                            <input class="form-control" id="trip_id" type="hidden" name="trip_id"  value="{{$data['Requirement']->id}}">
                                                <label class="col-md-3 col-form-label" for="vehicle_type">Vehicle Selection</label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="vehicle_type" name="vehicle_type"  {{ $data['VTId'] == '' ? '':'disabled' }}>
                                                        <option value="">Select Vehicle</option>
                                                    @foreach( $data['Vehicle'] as $value)
                                                        <option value="{{$value->id}}"  {{ $data['VTId'] == $value->id? 'selected':''}}>{{$value->registration_no}}</option>
                                                    @endforeach
                                                    </select>
                                                    @error('vehicle_type')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="driver">Driver</label>
                                                <div class="col-md-8">
                                                    <select class="form-control" id="driver" name="driver" {{ $data['DId'] == '' ? '':'disabled' }}>
                                                    <option value="">Select Driver</option>
                                                    @foreach(  $data['driver'] as $value)
                                                        <option value="{{$value->id}}" {{ $data['DId'] == $value->id? 'selected':''}}>{{$value->first_name}} {{$value->middle_name}} {{$value->last_name}}</option>
                                                    @endforeach
                                                    </select>
                                                    @error('driver')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="date">Tentative date and time of placement</label>
                                                <div class="col-md-4">
                                                    <input class="form-control" id="date" type="date" name="date" placeholder="date" value="{{$data['Requirement']->p_date}}" readonly>
                                                    <input class="form-control" id="id" type="hidden" name="id"  value="{{$data['Aid']}}">
                                                    @error('date')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-4">
                                                     
                                                    <input class="form-control" type="time" id="time" name="time" value="{{$data['Requirement']->p_time}}" readonly> 
                                                    @error('time')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            @if($data['Requirement']->trip_status == 'Approved')
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="driver">LR File</label>
                                                <div class="col-md-8">
                                                @if(count($data['ELR']) > 0)
                                                <a class="btn btn-sm btn-primary" href="{{url('edite-lr-file/'.$data['Requirement']->id)}}"  >Edit LR File</a>
                                                @else
                                                <a class="btn btn-sm btn-primary" href="{{url('create-lr-file/'.$data['Requirement']->id)}}"  >Create LR File</a>
                                                @endif
                                                <a class="btn btn-sm btn-info" href="javascript:void(0)"  onclick="window.open('{{ url('/Download_LR_File', $data['Requirement']->id) }}')" >Download LR File</a>
                                                </div>
                                            </div>
                                            @endif
                                    </div>
                                    @if($data['Requirement']->trip_status == 'Approved' || $data['Requirement']->trip_status == 'Declined')
                                    @else
                                    <div class="card-footer">
                                        <button class="btn btn-sm btn-success" type="submit"> Accept</button>

                                        <button class="btn btn-sm btn-danger"  type="button" onclick="reject_from({{$data['Requirement']->id}})"> Reject</button>
                                    </div>
                                    @endif
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                        </div>
                        <!-- /.row-->
                    </div>
                </div>
            </main>
<script type="text/javascript">
    function reject_from(id) {
        if(id)
        {
            $.ajax({
               type:"POST",
               url:"{{ url('/reject-trip-assign') }}",
               data:{"_token": "{{ csrf_token() }}", "id": id},
               success:function(result)
               {       
                   if(result == 'update')
                   {
                        location.reload();
                   }
                   else
                   {
                     alert('error');
                   }
               }
            });
        }
    }
</script>
@endsection