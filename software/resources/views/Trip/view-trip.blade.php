@extends('mainlayout')
@section('content')

<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> Trip Assign
						<div class="card-header-actions">
						  <a class="btn btn-sm btn-primary" type="submit" href="{{url('create-trip-form')}}"><i class="cil-plus"></i> Create Trip</a>
						</div>
					  </div>
					  <div class="card-body">
						  <div class="row">
							<div class="col-sm-12">
							  <table   id="example" class="table table-striped table-bordered table-responsive nowrap"  style="width:100%">
								<thead>
								  <tr role="row">
									<th >#
									</th>
									<th >Trip ID
									</th>
									<th  >Agreed Sales Value
									</th>
									<th  >Vendor Purchase Value
									</th>
									<th >Alloted To Vendor
									</th>
									<th  >Created
									</th>
									<th >Updated
									</th>
									
									<th >Assign Trip
									</th>
									
									<th >Trip closure
									</th>
									<th >Invoice Report
									</th>
									<th >Actions
									</th>
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
								
								@foreach($data['trip'] as $value)
								  <tr role="row" class="odd">
									<td>{{++$sn}}
									</td>
									<td class="sorting_1">{{$value->trip_id}}
									</td>
									<td>{{$value->agreed_sales_value}}
									</td>
									<td>{{$value->vendor_purchase_value}}
									</td>
									<td>@foreach($data['vendor'] as $vvalue) 
										@if($vvalue->vendor_id == $value->alloted_to_vendor_id) 
										{{$vvalue->name}}
										@endif
										@endforeach
									</td>
									<td>{{$value->created_at}}
									</td>
									<td>{{$value->updated_at}}
									</td>
									<td>
									  <a class="btn btn-success btn-sm" href="{{url('trip-assignments/'.$value->id)}}">
										<i class="cil-bus-alt"></i>
									  </a>
									</td>
									<td>
									  <a class="btn btn-success btn-sm" href="{{url('partial-trip-closure/'.$value->id)}}">
										<i class="cil-garage"></i>
									  </a>
									</td>
									<td>
									  <a class="btn btn-success btn-sm" href="{{url('view-invoice-report/'.$value->id)}}">
										<i class="cil-list-rich"></i>
									  </a>
									</td>
									<td>
									  <a class="btn btn-success btn-sm"  title="Edit" href="{{url('edit-client-info/'.$value->id)}}">
										<i class="cil-color-border"></i>
									  </a>
									</td>
								  </tr>
								@endforeach
								</tbody>
							  </table>
							</div>
						  </div>
					  </div>
					</div>
				  </div>
				</div>
            </main>
@endsection