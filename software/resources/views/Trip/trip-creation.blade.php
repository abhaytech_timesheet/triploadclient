@extends('mainlayout')
@section('content')  
            <main class="c-main">
                <div class="container-fluid">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
                    <div class="fade-in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Trip Creation Form</a></li>
                                        </ol>
                                    </nav>
                                    </div>
                                        <form class="form-horizontal" action="{{url('create-trip')}}" method="post" enctype="multipart/form-data">
                                    <div class="card-body">
                                        @csrf
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="agreed_sales_value">Trip</label>
                                                <div class="col-md-8">
                                                <select class="form-control @error('trip_id') is-invalid @enderror" id="trip_id" name="trip_id">
                                                        <option value=""  {{old('trip_id') == '' ? 'selected':''}}>Select Trip</option>
                                                    @foreach($data['trip'] as $value)
                                                        <option value="{{$value->trip_id}}"  {{old('trip_id') == $value->trip_id ? 'selected':''}}>{{$value->trip_id}}</option>
                                                    @endforeach
                                                    </select>  
                                                    @error('trip_id')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="agreed_sales_value">Vendor</label>
                                                <div class="col-md-8">
                                                <select class="form-control @error('vendor') is-invalid @enderror" id="vendor" name="vendor">
                                                        <option value=""  {{old('vendor') == '' ? 'selected':''}}>Select Vendor</option>
                                                    @foreach($data['vendor'] as $value)
                                                        <option value="{{$value->vendor_id}}"  {{old('vendor') == $value->vendor_id ? 'selected':''}}>{{$value->name}}</option>
                                                    @endforeach
                                                    </select>  
                                                    @error('vendor')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="agreed_sales_value">Agreed Sales value</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="agreed_sales_value" type="text"  name="agreed_sales_value" placeholder="Agreed Sales value">
                                                    @error('agreed_sales_value')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="vendor_purchase_value">Vendor Purchase Value</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="vendor_purchase_value" type="text" name="vendor_purchase_value" placeholder="Agreed Sales value">
                                                    @error('vendor_purchase_value')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            
 
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-sm btn-primary" type="submit" href="enter-freight-details.html"> Submit</button>
                                        <button class="btn btn-sm btn-danger" type="reset"> Reset</button>
                                    </div>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                        </div>
                        <!-- /.row-->
                    </div>
                </div>
            </main>

<script src="{{asset('js/jQuery-3.5.1.min.js')}}"></script>
<script type="text/javascript">
    $("#trip_id").change(function () {
        var id = this.value;

        if(id != null)
        {
           $.ajax({
               type:"POST",
               url:"{{ url('/get-trips')}}",
               data:{"_token": "{{ csrf_token() }}", "id":id },
               success:function(result)
               {   
                    if(result == 'DATA NOT FOUND')
                    {

                    }
                    else
                    {
                        $('#agreed_sales_value').val('')
                        $('#vendor_purchase_value').val('')
                        $('#vendor').val();
                        $('#agreed_sales_value').val(result['asv']);
                        $('#vendor_purchase_value').val(result['vpv']);
                        $('#vendor').val(result['atvi']);
                    }    

               }

            });
        }
        }); 
</script>
@endsection