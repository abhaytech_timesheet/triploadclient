@extends('mainlayout')
@section('content')
<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> Clients
						<div class="card-header-actions">
						  <a class="btn btn-sm btn-primary" type="submit" href="{{url('client-creation-form')}}"><i class="cil-plus"></i> Add New Client</a>
						</div>
					  </div>
					  <div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
						  <div class="row">
							<div class="col-sm-12 col-md-6">
							  <div class="dataTables_length" id="DataTables_Table_0_length">
								<label class="d-flex">Show 
								  <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="custom-select custom-select-sm form-control form-control-sm width10 ml-2 mr-2">
									<option value="10">10
									</option>
									<option value="25">25
									</option>
									<option value="50">50
									</option>
									<option value="100">100
									</option>
								  </select> entries
								</label>
							  </div>
							</div>
							<div class="col-sm-12 col-md-6">
							  <div id="DataTables_Table_0_filter " class="dataTables_filter pull-right"> 
								<label class="d-flex">Search:
								  <input type="search" class="form-control form-control-sm ml-2" placeholder="" aria-controls="DataTables_Table_0">
								</label>
							  </div>
							</div>
						  </div>
						  <div class="row">
							<div class="col-sm-12">
							  <table class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
								<thead>
								  <tr role="row">
									<th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" aria-sort="descending">#
									</th>
									<th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" aria-sort="descending">Enterprise name
									</th>
									<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" >Mobile Number
									</th>
									<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" >Email
									</th>
									<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">POC
									</th>
									<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" >Organization
									</th>
									<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Delivery Types
									</th>
									
									<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Delivery Types
									</th>
									<th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending">Actions
									</th>
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
								@foreach($data['clients'] as $value)
								  <tr role="row" class="odd">
									<td>{{++$sn}}
									</td>
									<td class="sorting_1">{{$value->enterprise_name}}
									</td>
									<td>{{$value->mobile_number}}
									</td>
									<td>{{$value->email}}
									</td>
									<td>{{$value->poc}}
									</td>
									<td>{{$value->organization_type}}
									</td>
									<td>{{$value->delivery_type}}
									</td>
									<td>{{$value->frequency}}
									</td>
									<td>
									  <a class="btn btn-success" href="{{url('edit-client-info/'.$value->id)}}">
										<i class="cil-color-border"></i>
									  </a>
									  <a class="btn btn-info"  href="{{url('view-client-info/'.$value->id)}}">
										<i class="cil-description"></i>
									  </a>
									</td>
								  </tr>
								@endforeach
								</tbody>
							  </table>
							</div>
						  </div>
						  <div class="row">
							<div class="col-sm-12 col-md-5">
							  <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 10 of 32 entries
							  </div>
							</div>
							<div class="col-sm-12 col-md-7">
							  <div class="dataTables_paginate paging_simple_numbers pull-right" id="DataTables_Table_0_paginate">
								<ul class="pagination">
								  <li class="paginate_button page-item previous disabled" id="DataTables_Table_0_previous">
									<a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link">Previous
									</a>
								  </li>
								  <li class="paginate_button page-item active">
									<a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0" class="page-link">1
									</a>
								  </li>
								  <li class="paginate_button page-item ">
									<a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0" class="page-link">2
									</a>
								  </li>
								  <li class="paginate_button page-item ">
									<a href="#" aria-controls="DataTables_Table_0" data-dt-idx="3" tabindex="0" class="page-link">3
									</a>
								  </li>
								  <li class="paginate_button page-item ">
									<a href="#" aria-controls="DataTables_Table_0" data-dt-idx="4" tabindex="0" class="page-link">4
									</a>
								  </li>
								  <li class="paginate_button page-item next" id="DataTables_Table_0_next">
									<a href="#" aria-controls="DataTables_Table_0" data-dt-idx="5" tabindex="0" class="page-link">Next
									</a>
								  </li>
								</ul>
							  </div>
							</div>
						  </div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
            </main>
@endsection