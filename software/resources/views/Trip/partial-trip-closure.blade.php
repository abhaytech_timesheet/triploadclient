@extends('mainlayout')
@section('content')
<style>
@media (min-width:768px){
/ width /
::-webkit-scrollbar {
width: 5px;
}

/ Track /
::-webkit-scrollbar-track {
background: #f1f1f1;
}

/ Handle /
::-webkit-scrollbar-thumb {
background: #3e6cc4;
}

/ Handle on hover /
::-webkit-scrollbar-thumb:hover {
background: #007bff;
}
.scroll-form {
overflow: scroll;
overflow-x: hidden;
height: auto;
max-height: calc(100vh - 151px);
padding-right: 10px;
}
}

.btn_upload{
width: 80px;
display: inline-block;
overflow: hidden;
position: relative;
color: #fff;
border: 1px solid #afaaaa;
background-image: url(images/photo.png);
background-position: center;
background-repeat: no-repeat;
height: 90%;
}
.btn_upload_vedio {
width: 80px;
display: inline-block;
overflow: hidden;
position: relative;
color: #fff;
border: 1px solid #afaaaa;
background-image: url(images/videocamera.png);
background-position: center;
background-repeat: no-repeat;
height: 90%;
}
.add-more-upload {
width: 80px;
display: inline-block;
overflow: hidden;
position: relative;
color: #fff;
border:1px solid #afaaaa;
background-image: url(images/plus.png);
background-position: center;
background-repeat: no-repeat;
height: 90%;
}

.btn_upload:hover,
.btn_upload:focus,
.btn_upload_vedio:hover,
.btn_upload_vedio:focus,
.add-more-upload:hover,
.add-more-upload:focus {
background-color: #007bff;
border: 1px solid #007bff;
}

.yes {
display: flex;
align-items: flex-start;
margin-top: 0px !important;
margin-right: 12px;
height: 90px;
}

.btn_upload input, .btn_upload_vedio input, .add-more-upload input {
cursor: pointer;
height: 100%;
position: absolute;
filter: alpha(opacity=1);
-moz-opacity: 0;
opacity: 0;
padding: 0;
width: 100%;
}

.it {
height: 100px;
max-width: 100px;
max-height: 100px;
position: absolute;
}

.btn-rmv1,
.btn-rmv2,
.btn-rmv3,
.btn-rmv4,
.btn-rmv5,
.btn-rmv6,
.btn-rmv7{
display: none;
}

.rmv {
cursor: pointer;
border-radius: 30px;
border: none;
display: inline-block;
background: rgb(255, 255, 255);
margin: -5px -10px;
background-image: url(images/traffic.png);
background-position: center;
background-repeat: no-repeat;
width: 20px;
height: 20px;
z-index: 1;
}


</style>
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Partial Trip Closure</a></li>
                                        </ol>
                                    </nav>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal" action="{{url('Trip_Closure_add')}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="txt_lr">LR No.</label>
                                                <div class="col-md-4">
                                                <input class="form-control" id="trip_id" type="hidden" name="trip_id" value="{{$data['TripAssign']->trip_id}}" readonly>
                                                    <input class="form-control" id="lr_number" type="text" name="lr_number" value="{{$data['TripAssign']->elr_num}}" placeholder="Autofill" readonly>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="width50-720px">
                                                            <div class="yes">
                                                                <span class="btn_upload">
                                                                    <input type="file" id="imag" name="lr_file" class="input-img" required>
                                                                </span>
                                                                <img id="ImgPreview" src="" class="preview1" />
                                                                <input type="button" id="removeImage1" value="" class="btn-rmv1" />
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="eway_bill">Ewaybill Number</label>
                                                <div class="col-md-4">
                                                    <input class="form-control" id="eway_bill" type="text" name="eway_bill" placeholder="Eway Bill Number" required>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="width50-720px">
                                                        <div class="yes">
                                                            <span class="btn_upload">
                                                                <input type="file" id="imag2" name="eway_bill_file" title="" required>
                                                            </span>
                                                            <img id="ImgPreview2" src="" class="preview2" />
                                                            <input type="button" id="removeImage2" value="" class="btn-rmv2" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="vehicle_no">Vehicle No.</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="vehicle_no" type="text" name="vehicle_no" value="{{$data['Vehicle']->vehicle_no}}" placeholder="Autofill"readonly> 
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="lr_date">LR Date</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="lr_date" type="text" name="lr_date"  value="{{date('d-M-y', strtotime($data['TripAssign']->created_at))}}"  placeholder="Autofill"readonly> 
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="from">From</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="from" type="text" name="from" value="{{$data['Requirement']->p_address}} {{$data['Requirement']->p_landmark}} {{ $data['PStates']->name}} {{ $data['PCity']->name}}" placeholder="Autofill"readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="to">To </label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="to" type="text" name="to"  value="{{$data['Requirement']->d_address}} {{$data['Requirement']->d_landmark}} {{ $data['DStates']->name}} {{ $data['DCity']->name}}" placeholder="Autofill"readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="count_of_shipment">Count Of Shipment</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="count_of_shipment" type="text" name="count_of_shipment" value="" placeholder="Count Of Shipment" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="invoice_value">Invoice Value</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" id="invoice_value" type="text" name="invoice_value" placeholder="Invoice Value" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">POD Received</label>
                                                <div class="col-md-3 col-form-label">
                                                    <div class="form-check checkbox">
                                                        <input class="form-check-input" id="OPD" name="OPD" type="checkbox" value="Yes" onchange="valueChanged()">
                                                        <label class="form-check-label" for="check1">Received</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-5" style="display: none;" id="opd_image">
                                                    <div class="width50-720px">        
                                                        <div class="yes">
                                                            <span class="btn_upload">
                                                            <input type="file" id="imag4" name="POD_file" title="">
                                                            </span>
                                                            <img id="ImgPreview4" src="" class="preview4" />
                                                            <input type="button" id="removeImage4" value="" class="btn-rmv4" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="damages">Damages </label>
                                                <div class="col-md-4">
                                                    <input class="form-control" id="damages" type="text" name="damages" placeholder="Damages">
                                                </div> 
                                                <div class="col-md-4">
                                                    <input type="file" name="damages_images[]" multiple="multiple">
                                                </div> 
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="fine">Fine</label>
                                                <div class="col-md-4">
                                                    <input class="form-control" id="fine" type="text" name="fine" placeholder="Fine Description">
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="width50-720px">    
                                                            <div class="yes">
                                                                <span class="btn_upload">
                                                                <input type="file" id="imag3" name="fine_image" title="">
                                                            
                                                                </span>
                                                                <img id="ImgPreview3" src="" class="preview3" />
                                                                <input type="button" id="removeImage3" value="" class="btn-rmv3" />
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        

                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-sm btn-primary" type="submit"> Submit</button>
                                        <button class="btn btn-sm btn-danger" type="reset"> Reset</button>
                                    </div>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                        </div>
                        <!-- /.row-->
                    </div>
                </div>
            </main>

<script src="{{asset('js/jQuery-3.5.1.min.js')}}"></script>
<script>

    function valueChanged()
    {
        if($('#OPD').is(":checked")) 
            $("#opd_image").show();
        else
            $("#opd_image").hide();
    }
    
    function readURL(input, imgControlName) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $(imgControlName).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#imag").change(function() {
  // add your logic to decide which image control you'll use
  var imgControlName = "#ImgPreview";
  readURL(this, imgControlName);
  $('.preview1').addClass('it');
  $('.btn-rmv1').addClass('rmv');
});
$("#imag2").change(function() {
  // add your logic to decide which image control you'll use
  var imgControlName = "#ImgPreview2";
  readURL(this, imgControlName);
  $('.preview2').addClass('it');
  $('.btn-rmv2').addClass('rmv');
});
$("#imag3").change(function() {
  // add your logic to decide which image control you'll use
  var imgControlName = "#ImgPreview3";
  readURL(this, imgControlName);
  $('.preview3').addClass('it');
  $('.btn-rmv3').addClass('rmv');
});
$("#imag4").change(function() {
  // add your logic to decide which image control you'll use
  var imgControlName = "#ImgPreview4";
  readURL(this, imgControlName);
  $('.preview4').addClass('it');
  $('.btn-rmv4').addClass('rmv');
});
$("#imag5").change(function() {
  // add your logic to decide which image control you'll use
  var imgControlName = "#ImgPreview5";
  readURL(this, imgControlName);
  $('.preview5').addClass('it');
  $('.btn-rmv5').addClass('rmv');
});
$("#imag6").change(function() {
  // add your logic to decide which image control you'll use
  var imgControlName = "#ImgPreview6";
  readURL(this, imgControlName);
  $('.preview6').addClass('it');
  $('.btn-rmv6').addClass('rmv');
});
$("#imag7").change(function() {
  // add your logic to decide which image control you'll use
  var imgControlName = "#ImgPreview7";
  readURL(this, imgControlName);
  $('.preview7').addClass('it');
  $('.btn-rmv7').addClass('rmv');
});

$("#removeImage1").click(function(e) {
  e.preventDefault();
  $("#imag").val("");
  $("#ImgPreview").attr("src", "");
  $('.preview1').removeClass('it');
  $('.btn-rmv1').removeClass('rmv');
});
$("#removeImage2").click(function(e) {
  e.preventDefault();
  $("#imag2").val("");
  $("#ImgPreview2").attr("src", "");
  $('.preview2').removeClass('it');
  $('.btn-rmv2').removeClass('rmv');
});
$("#removeImage3").click(function(e) {
  e.preventDefault();
  $("#imag3").val("");
  $("#ImgPreview3").attr("src", "");
  $('.preview3').removeClass('it');
  $('.btn-rmv3').removeClass('rmv');
});
$("#removeImage4").click(function(e) {
  e.preventDefault();
  $("#imag4").val("");
  $("#ImgPreview4").attr("src", "");
  $('.preview4').removeClass('it');
  $('.btn-rmv4').removeClass('rmv');
});
$("#removeImage5").click(function(e) {
  e.preventDefault();
  $("#imag5").val("");
  $("#ImgPreview5").attr("src", "");
  $('.preview5').removeClass('it');
  $('.btn-rmv5').removeClass('rmv');
});
$("#removeImage6").click(function(e) {
  e.preventDefault();
  $("#imag6").val("");
  $("#ImgPreview6").attr("src", "");
  $('.preview6').removeClass('it');
  $('.btn-rmv6').removeClass('rmv');
});
$("#removeImage7").click(function(e) {
  e.preventDefault();
  $("#imag7").val("");
  $("#ImgPreview7").attr("src", "");
  $('.preview7').removeClass('it');
  $('.btn-rmv7').removeClass('rmv');
});

    </script>
@endsection