<!DOCTYPE html>
<html>
<style>
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 3px 6px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 12px;
  margin: 1px 1px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}

.button1 {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
}

.button2:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
}

/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<head>

<link rel="stylesheet" href="{{asset('InvoiceFile/style.css')}}"> 

</head>
<body>
<div class="container">
	               @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
	    <form method="post" action="{{url('add-lr-file')}}" id="inForm">
        @csrf
<div class="row" style="display:inline-flex;width:100%;">
<div style="width:100%;margin-right:1px;">
	<table style="width:100%;">
	  <tr>
	   <td colspan="2"> <img src="{{asset('InvoiceFile/a-logo.png')}}"/></td>
	   <td colspan="10" rowspan ="2" style="padding: 5px;text-align: left;line-height: 20px;font-size: 12px;font-weight: 600;"> Advay Corporation<br/> Reg. Office: A-403,Pranit Chambers, Sakinaka, Andheri East, Mumbai,<br/>Maharashtra 400072<br/>
		<span>GST No:</span><br/>
		<span>CIN No:</span><br/>
		<span>PAN No:</span><br/>
	   </td>
	  </tr>
	  
	<tr>
	   <td colspan="2">Shipper Copy</td>
	</tr>
	<tr>
	  <td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">From: <span style=" font-weight: normal;"> {{$data['pl']->type}} </span></td>
	</tr>
	<tr>
	  <td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">Company Name :  <span style=" font-weight: normal;"> {{$data['pl']->company_name}} </span> </td>
	</tr>
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">GST No.</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Ph No.  <span style=" font-weight: normal;"> {{$data['t']->p_mob_no}} </span> </td>
	 
	</tr>
	<tr>
	  <td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">Address :  <span style=" font-weight: normal;"> {{$data['t']->p_address}} {{$data['t']->p_landmark}} </span> </td>
	  
	</tr>
	<tr>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">City : <span style=" font-weight: normal;">  @foreach($data['City'] as $cvalue) @if($data['t']->p_city == $cvalue->id) {{ $cvalue->name}}  @endif @endforeach </span></td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">Pincode : </td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">State :  <span style=" font-weight: normal;">   @foreach($data['States'] as $svalue) @if($data['t']->p_state == $svalue->id) {{ $svalue->name}}  @endif @endforeach </span></td>
	  
	</tr>
	</table>
	<table  id="myTable">
	<tr >
	  <td  class="weight800" style="padding-left:5px;height:28px;">S No.</td>
	  <td colspan="1" class="weight800" style="text-align:left;padding-left:5px;">Invoice No</td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">E-waybill No</td>
	  <td colspan="1" class="weight800" style="text-align:left;padding-left:5px;">Box Count</td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">Invoice Value </td>
	</tr>
	<tr>
	<input type="hidden" name="trip_id" value="{{$data['t']->id}}" required>
	  <td  class="weight800" style="padding-left:5px;height:25px;">1</td>
	  <td colspan="1" class="weight800" style="text-align:left;padding-left:5px;"><input type="text" name="invoice_no[]" id="invoice_no1" required> @error('invoice_no') <span class="help-block text-danger">{{ $message }}</span> @enderror </td>

	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;"><input type="text" name="e_waybill_no[]" id="e_waybill_no1" required> @error('e_waybill_no') <span class="help-block text-danger">{{ $message }}</span> @enderror </td>
	  <td colspan="1" class="weight800" style="text-align:left;padding-left:5px;"><input type="text" name="box_count[]" id="box_count1" required> @error('box_count') <span class="help-block text-danger">{{ $message }}</span> @enderror </td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px; display: flex;"><input type="text" name="invoice_value[]" id="invoice_value1" required><button class="button button2" type="button"  onclick="InputRow()">More</button> @error('invoice_value') <span class="help-block text-danger">{{ $message }}</span> @enderror </td>
	</tr>
	 <span id="InputField"></span>
	</table>
	<table  style="width:100%;">
<!-- 	</table>

	<table style="width:100%; margin-top:25px;"> -->
	  <tr>
	   <td colspan="12" class="weight800" style="text-align:center;padding-left:5px;height:28px;">Consignor Details</td>
	  </tr>
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Name</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   </td>
	</tr>
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Leaving Time</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style=" font-weight: normal;"> {{$data['t']->p_date}} {{$data['t']->p_time}} </span>  </td>
	</tr>
<!-- 	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Out-Date & Time</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr> -->
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Loading & Charges</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px; border-bottom:1px solid #fff0;height:28px;">Sign & Stamp</td>
	  <td colspan="5" class="weight800" style="text-align:center;padding-left:5px; border-bottom:1px solid #fff0;">Terms & Conditions</td>
	</tr>
	</table>
	<table style="width:100%; border-top:none;">
	<tr>
	  <td class="weight800" style="text-align:left;padding-left:5px;width:120px;height:20px;">Actual Wt.</td>
	  <td class="weight800" style="text-align:left;padding-left:5px;width:120px;height:20px;"></td>
	  <td rowspan="3" class="weight800" style="text-align:center;padding: 17px;font-size: 12px;">Movement of the groups under this LR will be governed by the contract between all concerned parties.</td>
	</tr>
	<tr>
	  <td  class="weight800" style="text-align:left;padding-left:5px;height:20px;">Volumetric Wt.</td>
	  <td  class="weight800" style="text-align:left;padding-left:5px;height:20px;"></td>
	</tr>
	<tr>
	  <td  class="weight800" style="text-align:left;padding-left:5px;height:20px;">Guaranteed Wt.</td>
	  <td  class="weight800" style="text-align:left;padding-left:5px;height:20px;"></td>
	</tr>
	</table>
</div>
<div style="width:100%;margin-left:1px;">
	<table style=" width:100%;">
		<tr>
			<td  class="weight800" style="padding-left:5px;">LR Date</td>
			<td  class="weight800" style="padding-left:5px;">Vechile No.</td>
			<td  class="weight800" style="padding-left:5px;">Driver No. </td>
		</tr>
		<tr>
			<td  class="weight800" style="padding-left:5px;height:127px;"><span style=" font-weight: normal;"> {{ date('d-m-Y', strtotime($data['TripAssign']->created_at))}} </span></td>
			<td  class="weight800" style="padding-left:5px;"> <span style=" font-weight: normal;"> {{$data['Vehicle']->registration_no}}</span></td>
			<td  class="weight800" style="padding-left:5px;"> <span style=" font-weight: normal;"> {{ $data['driver']->mobile_no}} </span></td>
		</tr>
	</table>
	<table style=" width:100%;">
		<tr>
			<td colspan="12" class="weight800" style="text-align:left;padding-left:5px;border-top:1px solid #0000;">To:  <span style=" font-weight: normal;"> {{$data['dl']->type}} </span></td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">Company :  <span style=" font-weight: normal;"> {{$data['dl']->company_name}} </span> </td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">GST No.</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Ph No. <span style=" font-weight: normal !important;"> {{$data['t']->d_mob_no}} </span></td>
		</tr> 
		<tr>
			<td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">Address :  <span style=" font-weight: normal;"> {{$data['t']->d_address}} {{$data['t']->d_landmark}} </span></td>
		</tr>
		<tr>
			<td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">City  <span style=" font-weight: normal;">  @foreach($data['City'] as $cvalue) @if($data['t']->d_city == $cvalue->id) {{ $cvalue->name}}  @endif @endforeach </span></td>
			<td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">Pincode</td>
			<td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">State :  <span style=" font-weight: normal;">   @foreach($data['States'] as $svalue) @if($data['t']->d_state == $svalue->id) {{ $svalue->name}}  @endif @endforeach </span></td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:28px;">Consignee Details</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Name</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
<!-- 		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">In-Date & Time</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr> -->
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Expected Arrival Time</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <span style=" font-weight: normal;"> {{$data['t']->d_date}} {{$data['t']->d_time}} </span></td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Unloading Charges</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:28px;">Damage Details</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="padding-left:5px;">Packing Damage</td>
			<td colspan="5" class="weight800" style="padding-left:5px;">Material Damage</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Count</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Count</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Value</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">SKU</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;"></td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Value</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:28px;">Consignee Sign & Stam</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:60px;"><img src="{{asset('InvoiceFile/image.jpg')}}" alt="stamp" width="10%" /></td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:28px;">Remarks</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:45px;"></td>
		</tr>
	</table>
</div>
</div>
        <center style="margin: 20px;"> <button class="button button2" type="submit" id="submit">Submit</button> 
        	<button  class="button button2" >Back</button> </center>
    </form>
</div>


<script src="{{asset('js/jQuery-3.5.1.min.js')}}"></script>
<script type="text/javascript">
var id = 1;
function InputRow(){

var markup = '';
id += 1;
    
if(id < 6 )
{
markup += '<tr id="'+id+'">';
markup += '<td  class="weight800" style="padding-left:5px;height:25px;">'+id+'</td>';
markup += '<td colspan="1" class="weight800" style="text-align:left;padding-left:5px;"><input type="text" name="invoice_no[]" id="invoice_no'+id+'" required> </td>';

markup += '<td colspan="3" class="weight800" style="text-align:left;padding-left:5px;"><input type="text" name="e_waybill_no[]" id="e_waybill_no'+id+'" required></td>';

markup += '<td colspan="1" class="weight800" style="text-align:left;padding-left:5px;"><input type="text" name="box_count[]" id="box_count'+id+'" required></td>';

markup += '<td colspan="3" class="weight800" style="text-align:left;padding-left:5px;"><input type="text" name="invoice_value[]" id="invoice_value'+id+'" required>';

markup += '&ensp; <button  type="button" id="del-btn' +id+ '" class="btn btn-sm btn-danger" onclick="$(this).parent().parent().remove();InputRow1();">-</button>';
markup += '</td>';
markup += '</tr>';
	
    //$('#InputField').append(markup);
    $('#myTable').append(markup);
}
};

function InputRow1(){

id -= 1;
 };

function  countFunction(id) {

    var myId = 0;
    var total = 0;

    var values = $("input[name='amount[]']")
              .map(function(){return $(this).val();}).get();

        var count = (values.length);

        for(myId=0; myId < values.length; ++myId )
        {   
            if(values[myId] != '')
            {
            total = parseInt(total) + parseInt(values[myId]);
            $('#allto').html(total);
            }
        }
     
}


</script>
</body>
</html>
