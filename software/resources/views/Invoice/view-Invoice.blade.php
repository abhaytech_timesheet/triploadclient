@extends('mainlayout')
@section('content')

<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> Invoice List
						<div class="card-header-actions">
						 <!--  <a class="btn btn-sm btn-primary" type="submit" href="{{url('create-trip-form')}}"><i class="cil-plus"></i> Create Trip</a> -->
						</div>
					  </div>
					  <div class="card-body">
						  <div class="row">
							<div class="col-sm-12">
							  <table   id="example" class="table table-striped table-bordered nowrap"  style="width:100%">
								<thead>
								  <tr role="row">
									<th >#
									</th>
									<th >Invoice No.
									</th>
									<th  >Invoice amount
									</th>
									<th  >Status
									</th>	
									<th  >Approve Status 
									</th>					
									<th >Actions
									</th>
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
								@foreach($data['in'] as $value)
								<tr>
									<td>{{++$sn}}</td>
									<td>{{$value->trip_id}}</td>
									<td>{{$value->invoice_no}}</td>
									<td>{{$value->status == 1? 'Active':'Inactive' }}</td>
									<td>{{$value->approve_status == 1? 'Approve':'Reject' }}</td>
									<td>
										<a class="btn btn-success btn-sm" href="{{url('generate-invoice/'.$value->in_id)}}">Generate Invoice</a>
										<a class="btn btn-info btn-sm" href="{{url('invoice-file/'.$value->in_id)}}">View</a>
										<a class="btn btn-warning btn-sm" href="{{url('edit-invoice/'.$value->in_id)}}">Edit</a>
										@if($value->approve_status == 0)
										<a class="btn btn-success btn-sm" href="{{url('ApproveAndReject-invoice-report/'.$value->in_id.'/1')}}">Approve</a>
										<a class="btn btn-danger btn-sm" href="{{url('ApproveAndReject-invoice-report/'.$value->in_id.'/0')}}">Reject</a>
										@endif
									</td>
								</tr>
								@endforeach
								</tbody>
							  </table>
							</div>
						  </div>
					  </div>
					</div>
				  </div>
				</div>
            </main>
@endsection