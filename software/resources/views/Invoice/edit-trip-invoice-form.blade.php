<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="{{asset('InvoiceFile/style.css')}}"> 
<style>
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 8px 6px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 12px;
  margin: 1px 1px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}

.button1 {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
}

.button2:hover {
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
}

/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
</head>
<body>
<div class="container">
    <form method="post" action="{{url('update-generate-invoice')}}" id="inForm" autocomplete="off">
        @csrf
	<table style="width:100%;">
	  <tr>
	   <td colspan="2"> <img src="{{asset('InvoiceFile/trip-logo.png')}}"/></td>
	   <td colspan="10" style="padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;"> ADVAY CORPORATION
        <br/> A-403, Pranik Chambers, Sakinaka, Mumbai - 400072. India.
        <br/>Mob No: +91 90660 80880. e-mail - accounts@advaycorp.in
		
	   </td>
	  </tr>
      <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">INVOICE NO</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: {{ $data['in']->invoice_no }}</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">PLACE OF SUPPLY</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: West Bengal</td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">INVOICE DATE
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 29/01/21 
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">LR NO
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: {{ $data['TripAssign']->elr_num}}
        </td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">E-WAY BILL NO</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">:  {{$data['in']->e_way_bill}}</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;"></td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">GSTIN</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 27AAFHV3053B1ZD</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;"></td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">DUE DATE</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 29/2/21</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;"></td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;border-bottom:none;">Bill To:</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;border-bottom:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">GSTIN</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 19AAECP0357J1Z9</td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;border-top:none;">Parmeshwari Polyplast (India) Pvt. Ltd.
            <br/>9, Jagmohan Mullick Lane
            <br/>4th Floor, Kolkata-700 007 (India)
            <br/>Phone: 91-33-2273-5107, 2271-9579
            <br/>Email: nextplast@gmail.com


        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;border-top:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;"></td>
	</tr>
	</table>
    <table id="myTable" style="width:100%;">
        <tr>
          <td colspan="1" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;">Sr. No.</td>
          <td colspan="2" style="padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;">PARTICULARS & DESCRIPTION</td>
          <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;">AMOUNT (INR)
        </td>
        </tr>
        @php $Rnumber = 0; $sum = 0; @endphp
        @foreach($data['InDesc'] as $key => $value)
        <tr>
            <input type="hidden" name="in_id" value="{{$data['in']->id}}">
            <td colspan="1" class="text-right padd5 lh20 fs14 weight600 b-btm-none"> {{$key+1}}</td>
            <td colspan="2" class="text-left padd5 lh20 fs14 weight600 b-btm-none">
                <input type="hidden" name="desc_id[]" value="{{$value->id}}">
                <textarea  name="description[]" id="description1" style="width: 99%; height: 40px;" placeholder="ENTER THE PARTICULARS & DESCRIPTION">{{$value->p_description}}</textarea>
            </td>
            <td colspan="2" class="text-right padd5 lh20 fs14 weight600 b-btm-none">
                <input type="number" step=".01" placeholder="ENTER AMOUNT" name="amount[]" value="{{$value->amount}}" id="amount1" style="width: 87%; height: 38px;" oninput="countFunction(this.id)" /> @if($key == 0) <button class="button button2" type="button"  onclick="InputRow()">More</button> @else &ensp; <button  type="button" class="btn btn-sm btn-danger">-</button> @endif
            </td>
        </tr>
        @php $Rnumber = $key; $sum += $value->amount; @endphp
        @endforeach
            <div id="InputField"></div>
    </table>
  

        <table width="100%">
                <tr>
                    <td colspan="1" class="text-right padd5 lh20 fs14 weight600 b-top-none"style="width: 133px;"></td>
                    <td colspan="2" class="text-left padd5 lh20 fs14 weight600 b-top-none" style="width: 282px;">Total Invoice Amount</td>
                    <td colspan="2" class="text-right padd5 lh20 fs14 weight600 b-top-none" ><input id="allto" value="{{$sum}}" disabled> </td>
                </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">A/C NAME</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: Advay Corporation</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;border-bottom:none;width:50%;">FOR ADVAY CORPORATION</td>

        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">BANK</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: ICICI Bank</td>
        <td colspan="2" rowspan="5" style="padding:5px;border-left:none;border-bottom:none;"><img src="{{asset('InvoiceFile/image.png')}}" alt="stamp" width="25%" /><br> <b>Authorised Signatory</b></td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">ACCOUNT NO
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 102205008247
        </td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">IFSC CODE
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: ICIC0001022
        </td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">BRANCH
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: Chandivali, Mumbai
        </td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">PAN NO
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: AAFHV3053B
        </td>
        </tr>
        </table>
        
    <table style="width:100%;">
        <tr style="border-top:none;">
            <td  style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-top:none;">T&C:				
                <br/>1. Interest @12% p.a. will be charged if the payment is not credited as per credit terms.				
                <br/>2. Any change or discrepancy in invoice is to be notified to us within 7 days of the receipt of invoice.				
                <br/>3. GST Expemted as per Goods and Service Tax Act.		
                </td>
        </tr> 
    </table>
        <center style="margin: 20px;"> <button class="button button2" type="submit" id="submit">Submit</button> <button  class="button button2" >Back</button> </center>
    </form>
</div>
</body>
<script src="{{asset('js/jQuery-3.5.1.min.js')}}"></script>
<script type="text/javascript">
var id = <?php echo $Rnumber+1; ?>

function InputRow(){

var markup = '';
id += 1;

markup += '<tr id="'+id+'">';
markup += '<td colspan="1" class="text-right padd5 lh20 fs14 weight600 b-btm-none">'+id+'</td>';
markup += '<td colspan="2" class="text-left padd5 lh20 fs14 weight600 b-btm-none">';
markup += '<textarea  name="description[]" id="description'+id+'" style="width: 99%; height: 40px;" required></textarea>';
markup += '</td>';

markup += '<td colspan="2" class="text-right padd5 lh20 fs14 weight600 b-btm-none">';

markup += '<input type="number" step=".01" id="amount'+id+'" value="0" oninput="countFunction(this.id)" name="amount[]" style="width: 87%; height: 38px;"  required>';

markup += '&ensp; <button  type="button" id="del-btn' +id+ '" class="btn btn-sm btn-danger" onclick="$(this).parent().parent().remove(); InputRow1();">-</button>';

markup += '</td>';
markup += '</tr>';
 var num = id;
    $('table tbody #InputField').append(markup);
    $('#myTable').append(markup);
};

function InputRow1(){

id -= 1;
 };

function  countFunction(id) {

    var myId = 0;
    var total = 0;

    var values = $("input[name='amount[]']")
              .map(function(){return $(this).val();}).get();

        var count = (values.length);

        for(myId=0; myId < values.length; ++myId )
        {
            if(values[myId] != '')
            {
                total = parseInt(total) + parseInt(values[myId]);
                $('#allto').val(total);
            }
        }
     
}



</script>

</html>
