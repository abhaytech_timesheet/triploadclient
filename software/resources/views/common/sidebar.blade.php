    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
        <div class="c-sidebar-brand" style="background-color:#fff;"><img class="c-sidebar-brand-full"
                src="{{asset('assets/brand/trip-load-logo.png')}}" width="118" height="46" alt="CoreUI Logo"><img
                class="c-sidebar-brand-minimized" src="{{asset('assets/brand/trip-load-logo.png')}}" width="118" height="46"
                alt="CoreUI Logo"></div>

        <ul class="c-sidebar-nav">
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="#">
                    <i class="cil-speedometer c-sidebar-nav-icon"></i>

                    Dashboard
                </a>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <span class="cil-calculator c-sidebar-nav-icon"></span>Settings</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Users</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Edit Menu</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Edit Menu
                            Elements</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Edit Roles</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Media</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Bread</a>
                    </li>
                </ul>
            </li>
            <li class="c-sidebar-nav-title">
                Master Data
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <span class="cil-user-follow c-sidebar-nav-icon"></span>Users</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Vendors</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{url('client-creation-form')}}"><span class="c-sidebar-nav-icon"></span>Clients</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{url('view-clients')}}"><span class="c-sidebar-nav-icon"></span> View Clients</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Drivers</a>
                    </li>
                </ul>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="#">
                    <i class="cil-notes c-sidebar-nav-icon"></i>
                    Manage Profile
                </a>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <span class="cil-truck c-sidebar-nav-icon"></span>Vehicles</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Vehicle Master</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Vehicle List</a>
                    </li>
                </ul>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <span class="cil-money c-sidebar-nav-icon"></span>Rate Master</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Rate Categories</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>FTL Rates</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>PTL Rates</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Master Rate Card</a>
                    </li>
                </ul>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <span class="cil-notes c-sidebar-nav-icon"></span>Client Requirements</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{url('client-requirement-form')}}"><span class="c-sidebar-nav-icon"></span>Client Requirement Form</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{url('view-clients-requirement')}}"><span class="c-sidebar-nav-icon"></span>View Client Requirements</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{url('create-trip-form')}}"><span class="c-sidebar-nav-icon"></span>Create Trip</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{url('view-trip')}}"><span class="c-sidebar-nav-icon"></span>View Trip</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="{{url('view-invoice-report')}}"><span class="c-sidebar-nav-icon"></span>View Invoice Report</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Vehicle Master</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Vehicle List</a>
                    </li>
                </ul>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <span class="cil-dollar c-sidebar-nav-icon"></span>Assigned Trip</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{url('view-assigned-trip')}}"><span class="c-sidebar-nav-icon"></span>Assigned Trip</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Vehicle List</a>
                    </li>
                </ul>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <span class="cil-dollar c-sidebar-nav-icon"></span>Business Opportunities</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Requirements</a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link" href="#"><span class="c-sidebar-nav-icon"></span>Vehicle List</a>
                    </li>
                </ul>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="#">
                    <i class="cil-truck c-sidebar-nav-icon"></i>
                    Trips
                </a>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="#">
                    <i class="cil-location-pin c-sidebar-nav-icon"></i>
                    Tracking
                </a>
            </li>
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="#">
                    <i class="cil-cash c-sidebar-nav-icon"></i>
                    Payments
                </a>
            </li>
        </ul>
        <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
    </div>