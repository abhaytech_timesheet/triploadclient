<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="{{asset('InvoiceFile/style.css')}}"> 

</head>
<body onload="window.print();">
<div class="container">
<div class="row" style="display:inline-flex;width:100%;">
<div style="width:100%;margin-right:1px;">
	<table style="width:100%;">
	  <tr>
	   <td colspan="2"> <img src="{{asset('InvoiceFile/a-logo.png')}}"/></td>
	   <td colspan="10" rowspan ="2" style="padding: 5px;text-align: left;line-height: 20px;font-size: 12px;font-weight: 600;"> Advay Corporation<br/> Reg. Office: A-403,Pranit Chambers, Sakinaka, Andheri East, Mumbai,<br/>Maharashtra 400072<br/>
		<span>GST No:</span><br/>
		<span>CIN No:</span><br/>
		<span>PAN No:</span><br/>
	   </td>
	  </tr>
	  
	<tr>
	   <td colspan="2">Shipper Copy</td>
	</tr>
	<tr>
	  <td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">From: <span style=" font-weight: normal;"> {{$data['pl']->type}} </span></td>
	</tr>
	<tr>
	  <td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">Company Name :  <span style=" font-weight: normal;"> {{$data['pl']->company_name}} </span> </td>
	</tr>
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">GST No.</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Ph No.  <span style=" font-weight: normal;"> {{$data['t']->p_mob_no}} </span> </td>
	 
	</tr>
	<tr>
	  <td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">Address :  <span style=" font-weight: normal;"> {{$data['t']->p_address}} {{$data['t']->p_landmark}} </span> </td>
	  
	</tr>
	<tr>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">City : <span style=" font-weight: normal;">  @foreach($data['City'] as $cvalue) @if($data['t']->p_city == $cvalue->id) {{ $cvalue->name}}  @endif @endforeach </span></td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">Pincode : </td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">State :  <span style=" font-weight: normal;">   @foreach($data['States'] as $svalue) @if($data['t']->p_state == $svalue->id) {{ $svalue->name}}  @endif @endforeach </span></td>
	  
	</tr>
	<tr>
	  <td  class="weight800" style="padding-left:5px;height:28px;">S No.</td>
	  <td colspan="1" class="weight800" style="text-align:left;padding-left:5px;">Invoice No</td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">E-waybill No</td>
	  <td colspan="1" class="weight800" style="text-align:left;padding-left:5px;">Box Count</td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">Invoice Value</td>
	</tr>
	@foreach($data['ELR'] as $key => $value)
	<tr>
	  <td  class="weight800" style="padding-left:5px;height:25px;">{{$key+1}}</td>
	  <td colspan="1" class="weight800" style="text-align:left;padding-left:5px;">{{$value->invoice_no}}</td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">{{$value->e_waybill}}</td>
	  <td colspan="1" class="weight800" style="text-align:left;padding-left:5px;">{{$value->box_count}}</td>
	  <td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">{{$value->invoice_val}}</td>
	</tr>
	@endforeach
<!-- 	</table>

	<table style="width:100%; margin-top:25px;"> -->
	  <tr>
	   <td colspan="12" class="weight800" style="text-align:center;padding-left:5px;height:28px;">Consignor Details</td>
	  </tr>
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Name</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    </td>
	</tr>
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Leaving Time</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style=" font-weight: normal;"> {{$data['t']->p_date}} {{$data['t']->p_time}} </span> </td>
	</tr>
<!-- 	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Out-Date & Time</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr> -->
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Loading & Charges</td>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
	  <td colspan="5" class="weight800" style="text-align:left;padding-left:5px; border-bottom:1px solid #fff0;height:28px;">Sign & Stamp</td>
	  <td colspan="5" class="weight800" style="text-align:center;padding-left:5px; border-bottom:1px solid #fff0;">Terms & Conditions</td>
	</tr>
	</table>
	<table style="width:100%; border-top:none;">
	<tr>
	  <td class="weight800" style="text-align:left;padding-left:5px;width:120px;height:20px;">Actual Wt.</td>
	  <td class="weight800" style="text-align:left;padding-left:5px;width:120px;height:20px;"></td>
	  <td rowspan="3" class="weight800" style="text-align:center;padding: 17px;font-size: 12px;">Movement of the groups under this LR will be governed by the contract between all concerned parties.</td>
	</tr>
	<tr>
	  <td  class="weight800" style="text-align:left;padding-left:5px;height:20px;">Volumetric Wt.</td>
	  <td  class="weight800" style="text-align:left;padding-left:5px;height:20px;"></td>
	</tr>
	<tr>
	  <td  class="weight800" style="text-align:left;padding-left:5px;height:20px;">Guaranteed Wt.</td>
	  <td  class="weight800" style="text-align:left;padding-left:5px;height:20px;"></td>
	</tr>
	</table>
</div>
<div style="width:100%;margin-left:1px;">
	<table style=" width:100%;">
		<tr>
			<td  class="weight800" style="padding-left:5px;">LR Date</td>
			<td  class="weight800" style="padding-left:5px;">Vechile No.</td>
			<td  class="weight800" style="padding-left:5px;">Driver No. </td>
		</tr>
		<tr>
			<td  class="weight800" style="padding-left:5px;height:127px;"><span style=" font-weight: normal;"> {{ date('d-m-Y', strtotime($data['TripAssign']->created_at))}} </span></td>
			<td  class="weight800" style="padding-left:5px;"> <span style=" font-weight: normal;"> {{$data['Vehicle']->registration_no}}</span></td>
			<td  class="weight800" style="padding-left:5px;"> <span style=" font-weight: normal;"> {{ $data['driver']->mobile_no}} </span></td>
		</tr>
	</table>
	<table style=" width:100%;">
		<tr>
			<td colspan="12" class="weight800" style="text-align:left;padding-left:5px;border-top:1px solid #0000;">To:  <span style=" font-weight: normal;"> {{$data['dl']->type}} </span></td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">Company :  <span style=" font-weight: normal;"> {{$data['dl']->company_name}} </span> </td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">GST No.</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Ph No. <span style=" font-weight: normal !important;"> {{$data['t']->d_mob_no}} </span></td>
		</tr> 
		<tr>
			<td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">Address :  <span style=" font-weight: normal;"> {{$data['t']->d_address}} {{$data['t']->d_landmark}} </span></td>
		</tr>
		<tr>
			<td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">City  <span style=" font-weight: normal;">  @foreach($data['City'] as $cvalue) @if($data['t']->d_city == $cvalue->id) {{ $cvalue->name}}  @endif @endforeach </span></td>
			<td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">Pincode</td>
			<td colspan="3" class="weight800" style="text-align:left;padding-left:5px;">State :  <span style=" font-weight: normal;">   @foreach($data['States'] as $svalue) @if($data['t']->d_state == $svalue->id) {{ $svalue->name}}  @endif @endforeach </span></td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:28px;">Consignee Details</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Name</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
<!-- 		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">In-Date & Time</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr> -->
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Expected Arrival Time</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <span style=" font-weight: normal;"> {{$data['t']->d_date}} {{$data['t']->d_time}} </span></td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Unloading Charges</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:28px;">Damage Details</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="padding-left:5px;">Packing Damage</td>
			<td colspan="5" class="weight800" style="padding-left:5px;">Material Damage</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Count</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Count</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Value</td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">SKU</td>
		</tr>
		<tr>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;"></td>
			<td colspan="5" class="weight800" style="text-align:left;padding-left:5px;">Value</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="text-align:left;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:28px;">Consignee Sign & Stam</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:60px;"><img src="{{asset('InvoiceFile/image.jpg')}}" alt="stamp" width="10%" /></td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:28px;">Remarks</td>
		</tr>
		<tr>
			<td colspan="12" class="weight800" style="padding-left:5px;height:45px;"></td>
		</tr>
	</table>
</div>
</div></div>

</body>
</html>
