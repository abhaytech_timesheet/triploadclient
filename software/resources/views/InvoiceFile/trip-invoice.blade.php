<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="{{asset('InvoiceFile/style.css')}}"> 

</head>
<body onload="window.print();">
<div class="container">


	<table style="width:100%;">
	  <tr>
	   <td colspan="2"> <img src="{{asset('InvoiceFile/trip-logo.png')}}"/></td>
	   <td colspan="10" style="padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;"> ADVAY CORPORATION
        <br/> A-403, Pranik Chambers, Sakinaka, Mumbai - 400072. India.
        <br/>Mob No: +91 90660 80880. e-mail - accounts@advaycorp.in
		
	   </td>
	  </tr>
	  
      <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">INVOICE NO</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">:  {{ $data['in']->invoice_no }}</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">PLACE OF SUPPLY</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: West Bengal</td>
       
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">INVOICE DATE
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 29/01/21
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">LR NO
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">:  {{ $data['TripAssign']->elr_num}}
        </td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">E-WAY BILL NO</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: {{$data['in']->e_way_bill}} </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;"></td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">GSTIN</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 27AAFHV3053B1ZD</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;"></td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">DUE DATE</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 29/2/21</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;"></td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;border-bottom:none;">Bill To:</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;border-bottom:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">GSTIN</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 19AAECP0357J1Z9</td>
	</tr>
    <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;border-top:none;">Parmeshwari Polyplast (India) Pvt. Ltd.
            <br/>9, Jagmohan Mullick Lane
            <br/>4th Floor, Kolkata-700 007 (India)
            <br/>Phone: 91-33-2273-5107, 2271-9579
            <br/>Email: nextplast@gmail.com


        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;border-top:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;"></td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;"></td>
	</tr>
	</table>
    <table style="width:100%;">
        <tr>
          <td colspan="1" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;">Sr. No.</td>
          <td colspan="2" style="padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;">PARTICULARS & DESCRIPTION</td>
          <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;">AMOUNT (INR)
        </td>
        </tr>
        @php $total = 0; $sn=0; @endphp
        @if($data['InDesc'] != '')
        @foreach($data['InDesc'] as $key => $value)
        <tr>
            <td colspan="1" style="text-align:right;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-bottom:none;">{{$key+1}}</td>
            <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-bottom:none;">{{$value->p_description}}

            <td colspan="2" style="text-align:right;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-bottom:none;"> {{$value->amount}} </td>
        </tr>
        <?php $total += $value->amount;  ?>
        @endforeach
        @endif
        <tr>
            <td colspan="1" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;"></td>
            <td colspan="2" style="text-align:right;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;" >Total Invoice Amount</td>
            <td colspan="2" style="text-align:right;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;" >{{$total}}</td>
        </tr>


<?php

$number = $total;
   $no = floor($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'one', '2' => 'two',
    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
    '7' => 'seven', '8' => 'eight', '9' => 'nine',
    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
    '13' => 'thirteen', '14' => 'fourteen',
    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
    '60' => 'sixty', '70' => 'seventy',
    '80' => 'eighty', '90' => 'ninety');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';
  if($points)
  {
        $p = "Paisa";
  }
  else
  {
    $p = "";
  }
 ?> 


        <tr>
            <td colspan="12" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-top:none;">TOTAL IN WORDS: <?php  echo ucwords($result) . "Rupees" . ucwords($points) . $p  ; ?> Only.</td>
        </tr>
    </table>


        <table width="100%">
        <tr>
            <td colspan="1" class="text-right padd5 lh20 fs14 weight600 b-top-none"style="width: 133px;"></td>
            <td colspan="2" class="text-left padd5 lh20 fs14 weight600 b-top-none" style="width: 282px;">Total Invoice Amount</td>
            <td colspan="2" class="text-right padd5 lh20 fs14 weight600 b-top-none" ><span id="allto"></span> </td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">A/C NAME</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: Advay Corporation</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;border-bottom:none;width:50%;">FOR ADVAY CORPORATION</td>

        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">BANK</td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: ICICI Bank</td>
        <td colspan="2" rowspan="5" style="padding:5px;border-left:none;border-bottom:none;"><img src="{{asset('InvoiceFile/image.png')}}" alt="stamp" width="25%" /> <br> <b>Authorised Signatory</b></td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">ACCOUNT NO
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: 102205008247
        </td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">IFSC CODE
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: ICIC0001022
        </td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">BRANCH
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: Chandivali, Mumbai
        </td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-right:none;">PAN NO
        </td>
        <td colspan="2" style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-left:none;">: AAFHV3053B
        </td>
        </tr>
        </table>

    <table style="width:100%;">
        <tr style="border-top:none;">
            <td  style="text-align:left;padding: 5px;line-height: 20px;font-size: 14px;font-weight: 600;border-top:none;">T&C:				
                <br/>1. Interest @12% p.a. will be charged if the payment is not credited as per credit terms.				
                <br/>2. Any change or discrepancy in invoice is to be notified to us within 7 days of the receipt of invoice.				
                <br/>3. GST Expemted as per Goods and Service Tax Act.	  			
                </td>
        </tr> 
    </table>
</div>
</body>
</html>
