@extends('mainlayout')
@section('content')

<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> View Trip
					  </div>
					  <div class="card-body">
						  <div class="row">
							<div class="col-sm-12">
							  <table   id="example" class="table table-striped table-bordered table-responsive nowrap"  style="width:100%">
								<thead>
								  <tr role="row">
									<th >#
									</th>
									<th >Trip ID
									</th>
									<th  >Agreed Sales Value
									</th>
									<th  >Vendor Purchase Value
									</th>
									<th >Vendor Name
									</th>
									<th  >Created
									</th>
									<th >Updated
									</th>
									<th >E-Way Bill</th>
									<th >LR</th>
									<th >Client Invoice</th>
									<th >Actions</th>
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
								
								@foreach($data['trip'] as $value)
								  <tr role="row" class="odd">
									<td>{{++$sn}}
									</td>
									<td class="sorting_1">{{$value->trip_id}}
									</td>
									<td>{{$value->agreed_sales_value}}
									</td>
									<td>{{$value->vendor_purchase_value}}
									</td>
									<td>@foreach($data['vendor'] as $vvalue) 
										@if($vvalue->vendor_id == $value->alloted_to_vendor_id) 
										{{$vvalue->name}}
										@endif
										@endforeach
									</td>
									<td>{{$value->created_at}}
									</td>
									<td>{{$value->updated_at}}
									</td>
									<td><img src="{{url('/images/Eway_Bill/'.$value->ewaybill_file)}}"></td>
									<td><img src="{{url('/images/LR/'.$value->lr_file)}}"></td>
									<td><img src="{{url('/images/InvoiceFile/'.$value->client_invoice_img)}}"></td>
									<td>
									  <a class="btn btn-success btn-sm" href="{{url('trip-view-info/'.$value->id)}}">
										<i class="fa fa-eye"></i>
									  </a>
									</td>
								  </tr>
								@endforeach
								</tbody>
							  </table>
							</div>
						  </div>
					  </div>
					</div>
				  </div>
				</div>
            </main>
@endsection