@extends('mainlayout')
@section('content')
            <main class="c-main">
                <div class="container-fluid">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
                    <div class="fade-in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Trip Information</a></li>
                                        </ol>
                                    </nav>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal" action="{{url('trip-assignment')}}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_trip_id">Trip ID</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_trip_id" type="text" name="txt_trip_id" placeholder="Autofill" value="{{$data['Requirement']->trip_id}}" readonly>
                                                </div>
                                        
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_from">From</label>
                                                <div class="col-md-3">
                                                    <textarea class="form-control" id="txt_from" type="text" name="txt_from" placeholder="Autofill" value="" readonly>{{$data['Requirement']->p_address}} {{$data['Requirement']->p_landmark}} {{ $data['States']->name}} {{$data['City']->name}}</textarea>
                                                </div>

                                                <label class="col-md-1 col-form-label"
                                                    for="txt_from">To</label>
                                                <div class="col-md-3">
                                                    <textarea class="form-control" id="txt_from" type="text" name="txt_from" placeholder="Autofill" value="" readonly>{{$data['Requirement']->d_address}} {{$data['Requirement']->p_landmark}} {{ $data['States']->name}} {{$data['City']->name}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_type_of_load">Type Of Load</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_type_of_load" type="text" name="txt_type_of_load" placeholder="Autofill"  value="{{$data['Requirement']->freight_type}}" readonly> 
                                                </div>

                                                <label class="col-md-1 col-form-label" for="txt_weight">Weight </label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_weight" type="text" name="txt_weight" placeholder="Autofill" value="{{$data['Requirement']->total_weight}}" readonly>
                                                </div>

                                                <label class="col-md-1 col-form-label" for="txt_truck_type">Truck Type </label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_truck_type" type="text" name="txt_truck_type" placeholder="Autofill" value="{{$data['VehicleType']->vehicle_type}}" readonly>
                                                </div>
                                                </div>
                                            <div class="form-group row">
                                            @php $sn=0; @endphp
                                            @foreach($data['DP'] as $value)
                                          
                                                <label class="col-md-1 col-form-label"for="txt_points_of_del">Del Points {{++$sn}} </label>
                                                <div class="col-md-3"style="margin-bottom: 14px;">
                                                    <input class="form-control" id="txt_points_of_del" type="text" name="txt_points_of_del" placeholder="Autofill" value="{{$value->delivery_point}}" readonly>
                                                </div>
                                            @endforeach 
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_type_of_shipment">Type of Shipment </label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_type_of_shipment" type="text" name="txt_type_of_shipment" placeholder="Autofill" value="{{$data['Requirement']->shipment_type}}" readonly>
                                                </div>
                                         
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_description">Material Desc</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_description" type="text" name="txt_description" placeholder="Autofill" value="{{$data['Requirement']->material_desc}}" readonly>
                                                </div>
                                       
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_gps">GPS</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_gps" type="text" name="txt_gps" placeholder="Autofill" value="{{$data['Requirement']->gps}}" readonly>
                                                </div>
                                   
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_load_unload">Loading/Unloading</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_load_unload" type="text" name="txt_load_unload" placeholder="Autofill" value="{{$data['Requirement']->loading_unloading}}" readonly>
                                                </div>
                                         
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_pickup_time">Pickup Date and Time</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_pickup_time" type="text" name="txt_pickup_time" placeholder="Autofill" value="{{$data['Requirement']->p_date}} {{$data['Requirement']->p_time}}" readonly>
                                                </div>
                                        

                                                <label class="col-md-1 col-form-label"
                                                    for="txt_dropup_time">Drop Date and Time</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_dropup_time" type="text" name="txt_dropup_time" placeholder="Autofill" value="{{$data['Requirement']->d_date}} {{$data['Requirement']->d_time}}" readonly>
                                                </div>
                                 
                                                <label class="col-md-1 col-form-label"
                                                    for="txt_comments">Additional Comments</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="txt_comments" type="text" name="txt_comments" placeholder="Autofill" value="{{$data['Requirement']->comments}}" readonly>
                                                </div>
                                           
                                            <input class="form-control" id="trip_id" type="hidden" name="trip_id"  value="{{$data['Requirement']->id}}">
                                                <label class="col-md-1 col-form-label" for="vehicle_type">Vehicle Selection</label>
                                                <div class="col-md-3">
                                                    <select class="form-control" id="vehicle_type" name="vehicle_type"  {{ $data['TripAssign']->vehicle_type_id == '' ? '':'disabled' }}>
                                                        <option value="">Select Vehicle</option>
                                                    @foreach( $data['Vehicle'] as $value)
                                                        <option value="{{$value->id}}"  {{ $data['TripAssign']->vehicle_type_id == $value->id? 'selected':''}}>{{$value->registration_no}}</option>
                                                    @endforeach
                                                    </select>
                                                    @error('vehicle_type')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                      
                                                <label class="col-md-1 col-form-label" for="driver">Driver</label>
                                                <div class="col-md-3">
                                                    <select class="form-control" id="driver" name="driver" {{ $data['TripAssign']->driver_id == '' ? '':'disabled' }}>
                                                    <option value="">Select Driver</option>
                                                    @foreach(  $data['driver'] as $value)
                                                        <option value="{{$value->id}}" {{ $data['TripAssign']->driver_id == $value->id? 'selected':''}}>{{$value->first_name}} {{$value->middle_name}} {{$value->last_name}}</option>
                                                    @endforeach
                                                    </select>
                                                    @error('driver')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                         
                                                <label class="col-md-1 col-form-label" for="date">Tentative date placement</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" id="date" type="date" name="date" placeholder="date" value="{{$data['Requirement']->p_date}}" readonly>
                                                    <input class="form-control" id="id" type="hidden" name="id"  value="{{$data['TripAssign']->id}}">
                                                    @error('date')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <label class="col-md-1 col-form-label" for="date">time</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" type="time" id="time" name="time" value="{{$data['Requirement']->p_time}}" readonly> 
                                                    @error('time')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>


                                                <label class="col-md-1 col-form-label" for="date">LR Number</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" type="text" id="time" name="time" value="{{$data['TripClosure']->lr_number}}" readonly> 
                                                </div>

                                                <label class="col-md-1 col-form-label" for="date">E-Way Bill Number</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" type="text" id="time" name="time" value="{{$data['TripClosure']->ewaybill_number}}" readonly> 
                                                </div>

                                                <label class="col-md-1 col-form-label" for="date">Vehicle Number</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" type="text" id="time" name="time" value="{{$data['TripClosure']->vehicle_number}}" readonly> 
                                                </div>

                                                <label class="col-md-1 col-form-label" for="date">Count Of Shipment</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" type="text" id="time" name="time" value="{{$data['TripClosure']->count_of_shipment}}" readonly> 
                                                </div>

                                                <label class="col-md-1 col-form-label" for="date">Invoice</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" type="text" id="time" name="time" value="{{$data['TripClosure']->invoice}}" readonly> 
                                                </div>

                                                <label class="col-md-1 col-form-label" for="date">Damages</label>
                                                <div class="col-md-3">
                                                    <textareas class="form-control" type="text" id="time" name="time" value="{{$data['TripClosure']->damages}}" readonly> </textarea>
                                                </div>

                                                <label class="col-md-1 col-form-label" for="date">Fine</label>
                                                <div class="col-md-3">
                                                    <input class="form-control" type="text" id="time" name="time" value="{{$data['TripClosure']->fine}}" readonly> 
                                                </div>

                                            </div>
                                            <center><a href="{{url('view-trip-info')}}" class="btn btn-success btn-sm">Back <i class="fa fa-angle-double-left"></i><i class="fa fa-angle-double-left"></i></a></center>
                                    </div>
                              
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                        </div>
                        <!-- /.row-->
                    </div>
                </div>
            </main>
@endsection