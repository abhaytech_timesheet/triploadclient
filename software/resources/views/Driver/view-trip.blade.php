@extends('mainlayout')
@section('content')

<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> Active Trip</div>
					  <div class="card-body">
						  <div class="row">
							<div class="col-sm-12">
							  <table   id="example" class="table table-striped table-bordered table-responsive nowrap"  style="width:100%">
								<thead>
								  <tr role="row">
									<th >#
									</th>
									<th >Trip ID
									</th>
									<th  >Agreed Sales Value
									</th>
									<th  >Vendor Purchase Value
									</th>
									<th >Alloted To Vendor
									</th>
									<th  >Created
									</th>
									<th >Updated
									</th>
									@if(Route::current()->uri() == 'view-assigned-trip')
									<th >Upload Invoice</th>
									<th >Action</th>
									@endif
									@if(Route::current()->uri() == 'view-active-trip')

									<th >E-way Bill</th>
									<th >LR</th>
									<th >Client Invoice</th>
									<th >Trip Closure</th>
									@endif
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
								
								@foreach($data['trip'] as $value)
								  <tr role="row" class="odd">
									<td>{{++$sn}}
									</td>
									<td class="sorting_1">{{$value->trip_id}}
									</td>
									<td>{{$value->agreed_sales_value}}
									</td>
									<td>{{$value->vendor_purchase_value}}
									</td>
									<td>@foreach($data['vendor'] as $vvalue) 
										@if($vvalue->vendor_id == $value->alloted_to_vendor_id) 
										{{$vvalue->name}}
										@endif
										@endforeach
									</td>
									<td>{{$value->created_at}}
									</td>
									<td>{{$value->updated_at}}
									</td>
									@if(Route::current()->uri() == 'view-assigned-trip')
									<td>
									    <button type="button" class="btn-sm btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$value->id}}"> Upload </button>

									    <!-- Modal -->
										<div class="modal fade" id="exampleModal{{$value->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
										  <div class="modal-dialog">
										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title" id="exampleModalLabel">Upload  E-way Bill, LR and client Invoice</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										                                    <form class="form-horizontal" action="{{url('upload-bills')}}" method="post" id="invoice{{$value->id}}" enctype="multipart/form-data">
										      <div class="modal-body">
										                                            @csrf
										                                    <div class="card-body">
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label" for="ewayfill">E-way Bill</label>
										                                                <div class="col-md-8">
										                                                	<input type="hidden" name="trip_id" id="trip_id" value="{{$value->id}}">
										                                                    <input class="form-control @error('ewayfill') is-invalid @enderror" id="ewayfill" type="file"
										                                                        name="ewayfill" placeholder="File" value="{{old('ewayfill')}}">
										                                                    @error('ewayfill')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-ewayfill"></span>
										                                                </div>
										                                            </div>
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label" for="lr">LR </label>
										                                                <div class="col-md-8">
										                                                    <input class="form-control @error('lr') is-invalid @enderror" id="lr" type="file" name="lr" placeholder="LR file"  value="{{old('lr')}}">
										                                                    @error('lr')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-lr"></span>
										                                                </div>
										                                            </div>
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label"
										                                                    for="client-input">client Invoice</label>
										                                                <div class="col-md-8">
										                                                    <input class="form-control @error('client_invoice') is-invalid @enderror" id="client_invoice" type="file" name="client_invoice" placeholder="File"  value="{{old('client_invoice')}}">
										                                                    @error('client_invoice')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-client_invoice"></span>
										                                                </div>
										                                            </div> 
										                                    </div>
										                                   

										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										        <button type="button" onclick="ebill_from('invoice{{$value->id}}')" class="btn btn-primary">Save changes</button>
										      </div>
										       </form>
										    </div>
										  </div>
										</div>


									</td>
									<td>
										<button 
										@if($value->lr_file =='' ||  $value->ewaybill_file =='' || $value->client_invoice_img == '')
										  onclick="show_message()"
										  class="btn-sm btn btn-danger"
										@else
										class="btn-sm btn btn-success"
										@endif
										> Start Trip  
									</button>
									</td>

									@endif

									@if(Route::current()->uri() == 'view-active-trip')
									<td><a class="btn btn-success btn-sm" onclick="window.open('{{url('/images/Eway_Bill/'.$value->ewaybill_file)}}'); return false"> <i class="far fa-list-alt"></i></a> </td>
									<td><a class="btn btn-success btn-sm" onclick="window.open('{{url('/images/InvoiceFile/'.$value->client_invoice_img)}}'); return false"> <i class="far fa-list-alt"></i></a> </td>
									<td><a class="btn btn-success btn-sm" onclick="window.open('{{url('/images/LR/'.$value->lr_file)}}'); return false"> <i class="far fa-list-alt"></i></a> </td>
									<td>
									    <button type="button" class="btn-sm btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$value->id}}"> Upload </button>

									    <!-- Modal -->
										<div class="modal fade" id="exampleModal{{$value->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
										  <div class="modal-dialog">
										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title" id="exampleModalLabel">Shipment Details</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										        <form class="form-horizontal" action="{{url('trip-closure-update')}}" method="post" id="invoice{{$value->id}}" enctype="multipart/form-data">
										      <div class="modal-body">
										                                            @csrf
										                                    <div class="card-body">
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label" for="shipment">Shipment Count</label>
										                                                <div class="col-md-8">
										                                                	<input type="hidden" name="trip_id" id="trip_id" value="{{$value->id}}">
										                                                    <input type="number" class="form-control @error('shipment') is-invalid @enderror" id="shipment" name="shipment" placeholder="Shipment Details" value="{{old('shipment')}}">
										                                                    @error('shipment')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-address"></span>
										                                                </div>
										                                            </div>
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label" for="pod">POD </label>
										                                                <div class="col-md-8">
										                                                    <input class="form-control @error('pod') is-invalid @enderror" id="pod" type="file" name="pod" placeholder="LR file"  value="{{old('pod')}}">
										                                                    @error('pod')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-pod"></span>
										                                                </div>
										                                            </div>
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label" for="damages">Damages </label>
										                                                <div class="col-md-8">
										                                                    <textarea class="form-control @error('damages') is-invalid @enderror" id="damages" type="file" name="damages" placeholder="Damages"  value="{{old('damages')}}"></textarea>
										                                                    @error('damages')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-damages"></span>
										                                                </div>
										                                            </div>
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label" for="lr">Damages File </label>
										                                                <div class="col-md-8">
										                                                    <input class="form-control @error('lr') is-invalid @enderror" id="lr" type="file" name="lr" placeholder="LR file"  value="{{old('lr')}}">
										                                                    @error('lr')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-lr"></span>
										                                                </div>
										                                            </div>
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label"
										                                                    for="client-input">Fine</label>
										                                                <div class="col-md-8">
										                                                    <input class="form-control @error('fine') is-invalid @enderror" id="fine" type="number" name="fine" placeholder="File"  value="{{old('fine')}}">
										                                                    @error('fine')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-fine"></span>
										                                                </div>
										                                            </div> 
										                                            <div class="form-group row">
										                                                <label class="col-md-4 col-form-label"
										                                                    for="client-input">Fine File</label>
										                                                <div class="col-md-8">
										                                                    <input class="form-control @error('fine_file') is-invalid @enderror" id="fine_file" type="file" name="fine_file" placeholder="File"  value="{{old('fine_file')}}">
										                                                    @error('fine_file')
										                                                    <span class="help-block text-danger">{{ $message }}</span>
										                                                    @enderror
										                                                    <span class="help-block text-danger" id="div-fine_file"></span>
										                                                </div>
										                                            </div> 
										                                    </div>
										                                   

										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										        <button type="button" onclick="ebill_from('invoice{{$value->id}}')" class="btn btn-primary">Save changes</button>
										      </div>
										       </form>
										    </div>
										  </div>
										</div>

									</td>
									@endif
								  </tr>
								@endforeach
								</tbody>
							  </table>
							</div>
						  </div>
					  </div>
					</div>
				  </div>
				</div>



</main>
@if(Route::current()->uri() == 'view-assigned-trip')
<script type="text/javascript">

    function ebill_from(id) {

	 var ewayfill = $('#ewayfill').val().replace("C:\\fakepath\\", "");
	 var lr = $('#lr').val().replace("C:\\fakepath\\", "");
	 var client_invoice = $('#client_invoice').val().replace("C:\\fakepath\\", "");
	 var trip_id = $('#trip_id').val();

	 	if(ewayfill =='')
        {
        	$('#div-ewayfill').html('This field is required');
        }
        else
        {
			$('#div-ewayfill').html('');
        }
	 	if(lr =='')
        {
        	$('#div-lr').html('This field is required');
        }
        else
        {
			$('#div-lr').html('');
        }
	 	if(client_invoice =='')
        {
        	$('#div-client_invoice').html('This field is required');
        }
        else
        {
			$('#div-client_invoice').html('');
        }
        if(ewayfill !='' && lr !='' && client_invoice !='' && trip_id !='')
        {
        	document.getElementById(id).submit();
        }

    }

 	function show_message() {
    	alert('You cannot start the trip until you upload the documents required.');
    }
</script>
@endif

@if(Route::current()->uri() == 'view-active-trip')
<script type="text/javascript">

    function ebill_from(id) {

     var pod = $('#pod').val().replace("C:\\fakepath\\", "");
	 var lr = $('#lr').val().replace("C:\\fakepath\\", "");
	 var fine_file = $('#fine_file').val().replace("C:\\fakepath\\", "");
	 var shipments = $('#shipments').val();
	 var damages = $('#damages').val();
	 var fine = $('#fine').val();
	 var trip_id = $('#trip_id').val();





	 	if(pod =='')
        {
        	$('#div-pod').html('This field is required');
        }
        else
        {
			$('#div-pod').html('');
        }
       	if(shipments =='')
        {
        	$('#div-shipments').html('This field is required');
        }
        else
        {
			$('#div-shipments').html('');
        }
       	/*
	 	if(lr =='')
        {
        	$('#div-lr').html('This field is required');
        }
        else
        {
			$('#div-lr').html('');
        }
	 	if(fine_file =='')
        {
        	$('#div-fine_file').html('This field is required');
        }
        else
        {
			$('#div-fine_file').html('');
        }
	 	if(damages =='')
        {
        	$('#div-damages').html('This field is required');
        }
        else
        {
			$('#div-damages').html('');
        }
	 	if(fine =='')
        {
        	$('#div-fine').html('This field is required');
        }
        else
        {
			$('#div-fine').html('');
        }*/
        if(shipments !='' && trip_id !=''&& pod !='') //&& lr !='' && fine_file !='' && damages !='' && fine !='' &&
        {
        	document.getElementById(id).submit();
        }

    }
</script>
@endif


@endsection