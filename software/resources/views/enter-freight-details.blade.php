@extends('mainlayout')
@section('content')     
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Client Creation Form</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Enter Freight Details</li>
                                        </ol>
                                    </nav>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal" action="" method="post"
                                            enctype="multipart/form-data">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="total-weight">Total Weight</label>
                                                <div class="col-md-9">
                                                    <input class="form-control" id="text-input" type="text" name="total-weight" placeholder="Total Weight"><span class="help-block">Total Weight </span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="truck-type">Truck Type</label>
                                                <div class="col-md-9">
                                                    <input class="form-control" id="text-input" type="text" name="truck-type" placeholder="truck-type"><span class="help-block">DATA FETCH FROM DATABASE</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="select1">Type of Shipment</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" id="select1" name="organization-type">
                                                        <option value="0">Type of Shipment</option>
                                                        <option value="fragile">Fragile</option>
                                                        <option value="perishable">Perishable</option>
                                                        <option value="standard-shipping">Standard Shipping</option>
                                                    </select><span class="help-block">Please select Type of Shipment</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="select1">Type</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" id="select1" name="type">
                                                        <option value="0">Select Type</option>
                                                        <option value="ftlhomedel.">FTL </option>
                                                        <option value="ftlhomedel.">FTL Home Del.</option>
                                                        <option value="ecom">Ecom</option>
                                                    </select><span class="help-block">Please select type</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="discription">Material Description</label>
                                                <div class="col-md-9">
                                                    <input class="form-control" id="text-input" type="text" name="discription" placeholder="Material Description"><span class="help-block">Material Description</span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-sm btn-primary" type="submit" onclick="location.href='client-creation-4.html'"> Next</button>
                                        <button class="btn btn-sm btn-danger" type="reset"> Reset</button>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                        </div>
                        <!-- /.row-->
                    </div>
                </div>
            </main>
@endsection