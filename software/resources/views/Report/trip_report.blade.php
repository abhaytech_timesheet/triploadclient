@extends('mainlayout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('/css/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/datatable/responsive.bootstrap4.min.css')}}">
<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> Trip
						<div class="card-header-actions">
						  <a class="btn btn-sm btn-primary" type="submit" href="{{url('create-trip-form')}}"><i class="cil-plus"></i> Create Trip</a>
						</div>
					  </div>
					  <div class="card-body">
						  <div class="row">
							<div class="col-sm-12">
							  <table   id="example" class="table table-striped table-bordered table-responsive nowrap"  style="width:100%">
								<thead>
								  <tr role="row" >
									<th  rowspan="2">Trip No
									</th>
									<th colspan="2">
											Date
									</th>
									
									<th> Client
									</th>
									<th  >From
									</th>
									<th >To
									</th>
									<th>Way bill no.
									</th>
									<th  rowspan="2" >Vendor Name
									</th>
									
									<th rowspan="2">Vehicle No.
									</th>
									
									<th rowspan="2">LR no.
									</th>
									
									<th >Invoice
									</th>
									<th >SPOC
									</th>
								  </tr>
								  <tr>
								  	<td>
											Pickup
									</td>
								  	<td>
											Pickup
									</td>
								  	<td>
											Name
									</td>
								  	<td>
											City
									</td>
								  	<td>
											City
									</td>
								  	<td>
											No.
									</td>
								  	<td>
											No.
									</td>
								  	<td>
											No.
									</td>
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
					
								</tbody>
							  </table>
							</div>
						  </div>
					  </div>
					</div>
				  </div>
				</div>
            </main>
			
@endsection