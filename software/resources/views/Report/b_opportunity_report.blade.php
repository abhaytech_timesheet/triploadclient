@extends('mainlayout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('/css/datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/datatable/responsive.bootstrap4.min.css')}}">
<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> Opportunity
						<div class="card-header-actions">
						  <a class="btn btn-sm btn-primary" type="submit" href="{{url('create-trip-form')}}"><i class="cil-plus"></i> Create Trip</a>
						</div>
					  </div>
					      <div class="card-body">
                                        <table class="table table-responsive-sm table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Location From</th>
                                                    <th>Location To</th>
                                                    <th>Vehicle Type</th>
                                                    <th>Month</th>
                                                    <th>Year</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                                <tr>
                                                    <td><input type="text" class="form-control" id="location_from"
                                                            name="location_from" placeholder="Location From"></td>
                                                    <td><input type="text" class="form-control" id="location_to"
                                                            name="location_to" placeholder="Location To"></td>
                                                    <td><input type="text" class="form-control" id="vehicle_type"
                                                            name="vehicle_type" placeholder="Vehicle type"></td>
                                                    <td><input type="text" class="form-control" id="enter_month"
                                                            name="enter_month" placeholder="Enter Month"></td>
                                                    <td><input type="text" class="form-control" id="enter_year"
                                                            name="enter_year" placeholder="Enter Year"></td>
                                                    <td><input type="text" class="form-control" id="status"
                                                            name="status" placeholder="Status"></td>
                                                    <td>
                                                        <a href="" class="btn btn-primary" data-toggle="tooltip"
                                                            title="View"><i class="fa fa-search"></i></a>
                                                        
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
					  <div class="card-body">
						  <div class="row">
							<div class="col-sm-12">
							  <table   id="example" class="table table-striped table-bordered nowrap"  style="width:100%">
								<thead>
								  <tr role="row">
									<th >#
									</th>
									<th >Client Name
									</th>
									<th  >Date of Requirement
									</th>
									<th  >From
									</th>
									<th >To
									</th>
									<th>Vehicle Type
									</th>
									<th >Assigned to (Manager)
									</th>
									
									<th >Vendor Name
									</th>
									
									<th >Status
									</th>
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
					
								</tbody>
							  </table>
							</div>
						  </div>
					  </div>
					</div>
				  </div>
				</div>
            </main>
@endsection