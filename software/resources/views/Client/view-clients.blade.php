@extends('mainlayout')
@section('content')
<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> Clients
						<div class="card-header-actions">
						  <a class="btn btn-sm btn-primary" type="submit" href="{{url('client-creation-form')}}"><i class="cil-plus"></i> Add New Client</a>
						</div>
					  </div>
					  <div class="card-body">				
						  <div class="row">
							<div class="col-sm-12">
							  <table   id="example" class="table table-striped table-bordered table-responsive nowrap"  style="width:100%" >
								<thead>
								  <tr>
									<th >#
									</th>
									<th >Enterprise name
									</th>
									<th  >Mobile Number
									</th>
									<th  >Email
									</th>
									<th >POC
									</th>
									<th  >Organization
									</th>
									<th >Delivery Types
									</th>
									
									<th >Delivery Types
									</th>
									<th >GST No.
									</th>
									<th>Actions
									</th>
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
								@foreach($data['clients'] as $value)
								  <tr>
									<td>{{++$sn}}
									</td>
									<td class="sorting_1">{{$value->enterprise_name}}
									</td>
									<td>{{$value->mobile_number}}
									</td>
									<td>{{$value->email}}
									</td>
									<td>{{$value->poc}}
									</td>
									<td>{{$value->organization_type}}
									</td>
									<td>{{$value->delivery_type}}
									</td>
									<td>{{$value->frequency}}
									</td>
									<td>{{$value->gst}}
									</td>
									<td>
									  <a class="btn btn-success" href="{{url('edit-client-info/'.$value->id)}}">
										<i class="cil-color-border"></i>
									  </a>
									  <a class="btn btn-info"  href="{{url('view-client-info/'.$value->id)}}">
										<i class="cil-description"></i>
									  </a>
									</td>
								  </tr>
								@endforeach
								</tbody>
							  </table>
							</div>
						  </div>

					  </div>
					</div>
				  </div>
				</div>
            </main>
@endsection