@extends('mainlayout')
@section('content')
            <main class="c-main">
                <div class="container-fluid">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
                    <div class="fade-in">
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Client Requirement Form</a></li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        <form class="form-horizontal" action="{{url('update-clients-requirement')}}" method="post"
                                            enctype="multipart/form-data" id="requirement">
                                            @csrf
                            <div class="col-md-12 mb-2">
                                <div class="card">
                                    <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            
                                            <li class="breadcrumb-item active" aria-current="page">Enter Pick Up Details</li>
                                        </ol>
                                    </nav>
                                    </div>
                                    <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-4">
                                                <label class="" for="pickup_location">Location name</label>
                                                <input  id="id" type="hidden" name="id"  value="{{$data['Requirement']->id}}">                                                    
                                                    
                                                    <input class="form-control @error('pickup_location') is-invalid @enderror" id="pickup_location" type="text" name="pickup_location" placeholder="FETCH FROM DATABASE"  value="{{$data['Requirement']->p_location}}">                                                    
                                                    @error('pickup_location')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror

                                                    <input id="pickup_location_id" type="hidden" name="pickup_location_id" value="{{$data['Requirement']->p_location_id}}">
                                                    @error('pickup_location_id')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                    <div id="pickup_loc"> </div>                                                 
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class=""
                                                    for="pickup_address">Address</label>
                                                    <input class="form-control @error('pickup_address') is-invalid @enderror" id="pickup_address" type="text" name="pickup_address" placeholder="Enter your address"  value="{{$data['Requirement']->p_address}}">                                                    
                                                    @error('pickup_address')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="pickup_state">State</label>
                                                <select class="form-control @error('pickup_state') is-invalid @enderror" id="pickup_state" name="pickup_state">
                                                    <option value="" {{ $data['Requirement']->p_state == '' ? 'selected':''}}>Select State</option>
                                                @foreach($data['States'] as $value)
                                                    <option value="{{$value->id}}"  {{ $data['Requirement']->p_state == $value->id ? 'selected':''}}>{{$value->name}}</option>
                                                @endforeach
                                                    </select>                                                    
                                                    @error('pickup_state')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="pickup_city">City</label>
                                                <select class="form-control @error('pickup_city') is-invalid @enderror" id="pickup_city" name="pickup_city">
                                                    <option value="" >Select City</option>
                                                    @foreach($data['PCity'] as $pvalue)
                                                    <option value="{{$pvalue->id}}"  {{ $data['Requirement']->p_city == $pvalue->id ? 'selected':''}}>{{$pvalue->name}}</option>
                                                    @endforeach
                                                    </select>                                                 
                                                    @error('pickup_city')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class=""
                                                    for="pickup_landmark">Landmark</label>
                                                    <input class="form-control @error('pickup_landmark') is-invalid @enderror" id="pickup_landmark" type="text" name="pickup_landmark" placeholder="Enter your landmark"  value="{{$data['Requirement']->p_landmark}}">                                                    @error('pickup_landmark')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class=""
                                                    for="pickup_contact_person">Contact Person</label>
                                                    <input class="form-control @error('pickup_contact_person') is-invalid @enderror" id="pickup_contact_person" type="text" name="pickup_contact_person" placeholder="Enter contact person" value="{{$data['Requirement']->p_contact_person}}">                                                    @error('pickup_contact_person')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class=""
                                                    for="pickup_mob_no">Mobile Number</label>
                                                    <input class="form-control @error('pickup_mob_no') is-invalid @enderror" id="pickup_mob_no" type="text" maxlength="10" minlength="10" pattern="[0-9]{10}" name="pickup_mob_no" placeholder="Please enter your mobile number" value="{{$data['Requirement']->p_mob_no}}">                                                    @error('pickup_mob_no')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-->
                            <div class="col-md-12 mb-2">
                                <div class="card">
                                    <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item active" aria-current="page">Enter Drop Details</li>
                                        </ol>
                                    </nav>
                                    </div>
                                    <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-4">
                                                <label class=" col-form-label" for="drop_location">Location name</label>
                                                <input class="form-control @error('drop_location') is-invalid @enderror" id="drop_location" type="text" name="drop_location" placeholder="FETCH FROM DATABASE" value="{{$data['Requirement']->d_location}}">                                                    
                                                @error('drop_location')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                @enderror


                                                    <input id="drop_location_id" type="hidden" name="drop_location_id" value="{{$data['Requirement']->p_location_id}}">
                                                    @error('drop_location_id')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror

                                                <div id="drop_loc"> </div> 
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="drop_address">Address</label>
                                                <input class="form-control @error('drop_address') is-invalid @enderror" id="drop_address" type="text" name="drop_address" placeholder="Enter your address" value="{{$data['Requirement']->d_address}}">                                                    
                                                @error('drop_address')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="drop_state">State</label>
                                                <select class="form-control @error('drop_state') is-invalid @enderror" id="drop_state" name="drop_state">
                                                    <option value="">Select State</option>
                                                @foreach($data['States'] as $value)
                                                    <option value="{{$value->id}}"  {{$data['Requirement']->d_state == $value->id ? 'selected':''}}>{{$value->name}}</option>
                                                @endforeach
                                                    </select>                                                    
                                                    @error('drop_state')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            @php $sn=0; @endphp

                                            @foreach($data['DP'] as $value)
                                            @php $snum = ++$sn;  @endphp
                                            <div class="form-group col-md-4">
                                            <input type="hidden" name="drop_delivery_id[]" value="{{$value->id}}" require>     
                                                <label class="" for="drop_delivery_points{{$snum}}">Delivery Points {{$snum}}</label>
                                                    <input class="form-control" id="drop_delivery_points{{$snum}}" type="text" name="drop_delivery_points[]" placeholder="Delivery Points" value="{{$value->delivery_point}}">                                                    @error('drop_delivery_points')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                @if($sn == 1)
                                                <buttion type="buttion" class="btn btn-sm btn-primary" onclick="InputRow()">+</buttion> 
                                                @endif
                                            </div>
                                          
                                            @endforeach
                                            </div>
                                            <div class="row" id="InputField"></div>
                                            <div class="row">
                                            <div class="form-group col-md-4">
                                                <label class="" for="drop_city">City</label>
                                                <select class="form-control @error('drop_city') is-invalid @enderror" id="drop_city" name="drop_city">
                                                    <option value="">Select City</option>
                                                    @foreach($data['DCity'] as $dvalue)
                                                    <option value="{{$dvalue->id}}"  {{ $data['Requirement']->d_city == $dvalue->id ? 'selected':''}}>{{$dvalue->name}}</option>
                                                    @endforeach
                                                </select>                                                   
                                                @error('drop_city')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="drop_landmark">Landmark</label>
                                                    <input class="form-control @error('drop_landmark') is-invalid @enderror" id="drop_landmark" type="text" name="drop_landmark" placeholder="Enter your landmark" value="{{$data['Requirement']->d_landmark}}">                                                    
                                                    @error('drop_landmark')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class=""
                                                    for="drop_contact_person">Contact Person</label>
                                                    <input class="form-control @error('drop_contact_person') is-invalid @enderror" id="drop_contact_person" type="text" name="drop_contact_person" placeholder="Enter contact person" value="{{$data['Requirement']->d_contact_person}}">                                                    
                                                    @error('drop_contact_person')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class=""
                                                    for="drop_mob_no">Mobile Number</label>
                                                    <input class="form-control @error('drop_mob_no') is-invalid @enderror" id="drop_mob_no" type="text" maxlength="10" minlength="10" pattern="[0-9]{10}" name="drop_mob_no" placeholder="Please enter your mobile number" value="{{$data['Requirement']->d_mob_no}}">                                                    @error('drop_mob_no')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-->
                            <div class="col-md-12 mb-2">
                                <div class="card">
                                    <div class="card-header">
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item active" aria-current="page">Enter Freight Details</li>
                                        </ol>
                                    </nav>
                                    </div>
                                    <div class="card-body">
                                            <div class="row">
                                            <div class="form-group col-md-4">
                                                <label class="" for="freight_total_weight">Total Weight</label>
                                                    <input class="form-control @error('freight_total_weight') is-invalid @enderror" id="freight_total_weight" type="text" name="freight_total_weight" placeholder="Total Weight"  value="{{$data['Requirement']->total_weight}}">                                                    
                                                    @error('freight_total_weight')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class=""
                                                    for="freight_truck_type">Truck Type</label>
                                                    <select class="form-control @error('freight_truck_type') is-invalid @enderror" id="freight_truck_type" name="freight_truck_type">
                                                        <option value=""  {{old('freight_truck_type') == '' ? 'selected':''}}>Select Truck Type</option>
                                                    @foreach($data['VehicleType'] as $value)    
                                                        <option value="{{$value->id}}"  {{$data['Requirement']->truck_type == $value->id ? 'selected':''}}>{{$value->vehicle_type}}</option>
                                                    @endforeach
                                                    </select> 
                                                     @error('freight_truck_type')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="freight_shipment_type">Type of Shipment</label>
                                                    <select class="form-control @error('freight_shipment_type') is-invalid @enderror" id="freight_shipment_type" name="freight_shipment_type">
                                                        <option value=""  {{$data['Requirement']->shipment_type == '' ? 'selected':''}}>Type of Shipment</option>

                                                        <option value="1"  {{$data['Requirement']->shipment_type == 'Fragile' ? 'selected':''}}>Fragile</option>

                                                        <option value="2"  {{$data['Requirement']->shipment_type == 'Perishable' ? 'selected':''}}>Perishable</option>

                                                        <option value="3"  {{$data['Requirement']->shipment_type == 'Standard Shipping' ? 'selected':''}}>Standard Shipping</option>
                                                    </select>                                                    
                                                    @error('freight_shipment_type')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="" for="freight_type">Type</label>
                                                    <select class="form-control @error('freight_type') is-invalid @enderror" id="freight_type" name="freight_type">

                                                        <option value=""  {{$data['Requirement']->freight_type == '' ? 'selected':''}}>Select Type</option>

                                                        <option value="1"  {{$data['Requirement']->freight_type == 'FTL' ? 'selected':''}}>FTL</option>

                                                        <option value="2"  {{$data['Requirement']->freight_type == 'FTL Home Del.' ? 'selected':''}}>FTL Home Del.</option>

                                                        <option value="3"  {{$data['Requirement']->freight_type == 'Ecom' ? 'selected':''}}>Ecom</option>

                                                    </select>                                                    
                                                    @error('freight_type')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class=""
                                                    for="material_description">Material Description</label>
                                                    <input class="form-control @error('material_description') is-invalid @enderror" id="material_description" type="text" name="material_description" placeholder="Material Discription"  value="{{$data['Requirement']->material_desc}}">                                                    
                                                    @error('material_description')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                            </div>
                                            </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                            <div class="col-md-12 mb-2">
                                <div class="card">
                                    <div class="card-body">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">GPS</label>
                                                <div class="col-md-9 col-form-label">
                                                    <div class="form-check form-check-inline mr-1">
                                                        <input class="form-check-input" id="gps1" type="radio" value="yes" name="gps"   {{$data['Requirement']->gps == 'yes' ? 'checked':''}} >
                                                        <label class="form-check-label" for="gps">Yes</label>
                                                    </div>
                                                    <div class="form-check form-check-inline mr-1">
                                                        <input class="form-check-input" id="gps2" type="radio"
                                                            value="no" name="gps"   {{$data['Requirement']->gps == 'no' ? 'checked':''}} >
                                                        <label class="form-check-label" for="gps">No</label>
                                                    </div>                                                    @error('gps')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Loading/Unloading</label>
                                                <div class="col-md-9 col-form-label">
                                                    <div class="form-check form-check-inline mr-1">
                                                        <input class="form-check-input" id="yes" type="radio" value="yes" name="loading_unloading"  {{$data['Requirement']->loading_unloading == 'yes' ? 'checked':''}} >
                                                        <label class="form-check-label" for="yes">Yes</label>
                                                    </div>
                                                    <div class="form-check form-check-inline mr-1">
                                                        <input class="form-check-input" id="no" type="radio"
                                                            value="no" name="loading_unloading"  {{$data['Requirement']->loading_unloading == 'no' ? 'checked':''}}>
                                                        <label class="form-check-label" for="no">No</label>
                                                    </div>                                                    @error('loading_unloading')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="pickup_date">Pickup Date and Time</label>
                                                <div class="col-md-4">
                                                    <input class="form-control @error('pickup_date') is-invalid @enderror" id="pickup_date" type="date" name="pickup_date" placeholder="date"  value="{{$data['Requirement']->p_date}}">                                                    @error('pickup_date')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-4">
                                                    <input class="form-control @error('pickup_time') is-invalid @enderror" type="time" id="pickup_time" name="pickup_time"  value="{{$data['Requirement']->p_time}}">                                                    @error('pickup_time')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="date-input">Dropup Date and Time</label>
                                                <div class="col-md-4">
                                                    <input class="form-control @error('dropup_date') is-invalid @enderror" id="dropup_date" type="date" name="dropup_date" placeholder="date"  value="{{$data['Requirement']->d_date}}">                                                    @error('dropup_date')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-4">
                                                    <input class="form-control @error('dropup_time') is-invalid @enderror" type="time" id="dropup_time" name="dropup_time"  value="{{$data['Requirement']->d_time}}">                                                   
                                                     @error('dropup_time')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="additional_comments">Additional Comments</label>
                                                <div class="col-md-8">
                                                    <input class="form-control @error('additional_comments') is-invalid @enderror" id="additional_comments" type="text" name="additional_comments" placeholder="Additional Comments"  value="{{$data['Requirement']->comments}}">                                                    
                                                    @error('additional_comments')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                    <div class="card-footer">
                                        <button class="btn btn-sm btn-primary" type="submit" > Submit</button>
                                        <button class="btn btn-sm btn-danger" href="{{ url()->previous() }}"> Back</button>
                                    </div>
                                       
                                    </div>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                            </form>
                            
                            
                        </div>
                        <!-- /.row-->
                    </div>
                </div>
            </main>

<script src="{{asset('js/jQuery-3.5.1.min.js')}}"></script>
<script type="text/javascript">
    $("#pickup_state").change(function () {
        var id = this.value;
        if(this.value > 0)
        {
            $.ajax({
               type:"POST",
               url:"{{ url('/get-city')}}",
               data:{"_token": "{{ csrf_token() }}", "id":id },
               success:function(result)
               {   
                    if(result == 'DATA NOT FOUND')
                    {
                        alert(result);
                    }
                    else
                    {
                        $('#pickup_city').html(result);
                    }    

               }

            });
        }
        });    

    $("#drop_state").change(function () {
        var id = this.value;
        if(this.value > 0)
        {
            $.ajax({
               type:"POST",
               url:"{{ url('/get-city')}}",
               data:{"_token": "{{ csrf_token() }}", "id":id },
               success:function(result)
               {   
                    if(result == 'DATA NOT FOUND')
                    {
                        alert(result);
                    }
                    else
                    {
                        $('#drop_city').html(result);
                    }    

               }

            });
        }
        });

        $("#pickup_location").on('input', function () {
        var name = this.value;
        if(this.value != null)
        {
            $.ajax({
               type:"POST",
               url:"{{ url('/get-location-name')}}",
               data:{"_token": "{{ csrf_token() }}", "name":name },
               success:function(result)
               {   
                    if(result == 'DATA NOT FOUND')
                    {
                        console.log(result);
                        $('#pickup_loc').empty();
                    }
                    else
                    {
                    $('#pickup_loc').empty();
                    $.each(result,function(key,value){
                        $("#pickup_loc").append('<li onclick="select_pickup_loc('+result[key].id+')" id="'+result[key].id+'" class="list-group-item">'+result[key].company_name+' '+result[key].type+'</li>');
                    });
                    
                    }    

               }

            }); 
        }
        });


        
        $("#drop_location").on('input', function () {
        var name = this.value;
        if(this.value != null)
        {
            $.ajax({
               type:"POST",
               url:"{{ url('/get-location-name')}}",
               data:{"_token": "{{ csrf_token() }}", "name":name },
               success:function(result)
               {   
                    if(result == 'DATA NOT FOUND')
                    {
                        console.log(result);
                        $('#drop_loc').empty();
                    }
                    else
                    {
                        $('#drop_loc').empty();
                        $.each(result,function(key,value){
                            $("#drop_loc").append('<li onclick="select_drop_loc('+result[key].id+')" id="'+result[key].id+'" class="list-group-item">'+result[key].company_name+' '+result[key].type+'</li>');
                        });
                    
                    }    

               }

            }); 
        }
        });


    function select_pickup_loc(id)
    {
        $('#pickup_location').val($('#'+id).text());
         $('#pickup_location_id').val(id);
        $('#pickup_loc').empty();
    }

    function select_drop_loc(id)
    {
        $('#drop_location').val($('#'+id).text());
        $('#drop_location_id').val(id);
        $('#drop_loc').empty();
    }

    
var id = {{$sn}};
function InputRow(){
var markup = '';
id += 1;
markup += '<div class="form-group col-md-4" id="' +id+ '">';
markup += '<label class="" for="drop_delivery_points' +id+ '">Delivery Points ' +id+ '</label>';
markup += '<input type="text" id="drop_delivery_points' +id+ '" name="drop_delivery_points[]" class="form-control"  placeholder="Delivery Points" required>';
markup += '<button  type="button" id="del-btn' +id+ '" class="btn btn-sm btn-danger" onclick="$(this).parent().remove();  InputRow1();">-</button>';
markup += '</div>';
$('#InputField').append(markup);
};

function InputRow1(){

id -= 1;
 };
</script>

@endsection