@extends('mainlayout')
@section('content')
     <main class="c-main">
                <div class="container-fluid">
                @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
                    <div class="fade-in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header"><strong>Client Creation</strong> Form</div>
                                        <form class="form-horizontal" action="{{url('update-client-creation')}}" method="post" enctype="multipart/form-data">
                                            @csrf
                                    <div class="card-body">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="enterprise-name">Enterprise name</label>
                                                <div class="col-md-9">
                                                <input id="id" type="hidden" name="id" value="{{$data['clients']->id}}">
                                                    <input class="form-control @error('enterprise_name') is-invalid @enderror" id="enterprise_name" type="text"
                                                        name="enterprise_name" placeholder="Enter your enterprise name" value="{{$data['clients']->enterprise_name}}">
                                                    @error('enterprise_name')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="poc">POC</label>
                                                <div class="col-md-9">
                                                    <input class="form-control @error('poc') is-invalid @enderror" id="poc" type="text" name="poc" placeholder="POC"  value="{{$data['clients']->poc}}">
                                                    @error('poc')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="client-input">Address</label>
                                                <div class="col-md-9">
                                                    <input class="form-control @error('address') is-invalid @enderror" id="address" type="text" name="address" placeholder="Enter your address"  value="{{$data['clients']->address}}">
                                                    @error('address')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="organization_type">Type of Organization</label>
                                                <div class="col-md-9">
                                                    <select class="form-control @error('organization_type') is-invalid @enderror" id="organization_type" name="organization_type">
                                                        <option value="" {{$data['clients']->organization_type == '' ? 'selected':''}} >Select organization type</option>

                                                        <option value="1" {{$data['clients']->organization_type == 'Public Ltd' ? 'selected':''}}>Public Ltd</option>

                                                        <option value="2" {{$data['clients']->organization_type == 'Private Ltd' ? 'selected':''}}>Private Ltd</option>

                                                        <option value="3" {{$data['clients']->organization_type == 'LLP' ? 'selected':''}}>LLP</option>

                                                    </select>
                                                    @error('organization_type')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="delivery_type">Delivery Types</label>
                                                <div class="col-md-9">
                                                    <select class="form-control @error('delivery_type') is-invalid @enderror" id="delivery_type" name="delivery_type">
                                                        <option value=""  {{$data['clients']->delivery_type == '' ? 'selected':''}}>Select delivery types</option>

                                                        <option value="1"  {{$data['clients']->delivery_type == 'FTL' ? 'selected':''}}>FTL</option>

                                                        <option value="2"  {{$data['clients']->delivery_type == 'FTL Home Del.' ? 'selected':''}}>FTL Home Del.</option>

                                                        <option value="3"  {{$data['clients']->delivery_type == 'Ecom' ? 'selected':''}}>Ecom</option>

                                                    </select>
                                                    @error('delivery_type')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="frequency">Frequency</label>
                                                <div class="col-md-9">
                                                    <select class="form-control @error('frequency') is-invalid @enderror" id="frequency" name="frequency">
                                                        <option value=""  {{$data['clients']->frequency == '' ? 'selected':''}}>Select frequency</option>
                                                        <option value="1"  {{$data['clients']->frequency == 'One Time' ? 'selected':''}}>One Time</option>
                                                        <option value="2"  {{$data['clients']->frequency == 'Monthly' ? 'selected':''}}>Monthly</option>
                                                    </select>
                                                    @error('frequency')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="client-number">Mobile Number</label>
                                                <div class="col-md-9">
                                                    <input class="form-control @error('mobile_number') is-invalid @enderror" id="mobile_number" type="text" maxlength="10" minlength="10" pattern="[0-9]{10}" name="mobile_number" placeholder="Please enter your mobile number"  value="{{$data['clients']->mobile_number}}" readonly="true">
                                                    @error('mobile_number')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="client-number">Email</label>
                                                <div class="col-md-9">
                                                    <input class="form-control @error('email') is-invalid @enderror" id="email" type="text" name="email" placeholder="Please enter your email"  value="{{$data['clients']->email}}" readonly="true">
                                                    @error('email')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"
                                                    for="client-number">GST</label>
                                                <div class="col-md-9">
                                                    <input class="form-control @error('gst') is-invalid @enderror" id="gst" type="text" name="gst" placeholder="Please enter your GST No."  value="{{$data['clients']->gst}}" >
                                                    @error('gst')
                                                    <span class="help-block text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-sm btn-primary" type="submit"> Submit</button>
                                        <button class="btn btn-sm btn-danger" href="{{ url()->previous() }}"> Back</button>
                                    </div>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- /.col-->
                        </div>
                        <!-- /.row-->
                    </div>
                </div>
            </main>
@endsection