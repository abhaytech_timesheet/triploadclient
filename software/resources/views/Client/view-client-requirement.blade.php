@extends('mainlayout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('/css/datatable/responsive.bootstrap4.min.css')}}">
<main class="c-main">
                <div class="container-fluid">
				@if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"><strong>{{session('success')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @elseif(session('warning'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>{{session('warning')}}</strong>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                @endif
				  <div class="fade-in">
					<div class="card">
					  <div class="card-header"> Clients Requirement
						<div class="card-header-actions">
						  <a class="btn btn-sm btn-primary" type="submit" href="{{url('client-requirement-form')}}"><i class="cil-plus"></i> Client Requirement Form</a>
						</div>
					  </div>
					  <div class="card-body">

						  <div class="row">
							<div class="col-sm-12">
							  <table  id="example" class="table table-striped table-bordered table-responsive nowrap"  style="width:100%" >
								<thead>
								  <tr role="row">
									<th >Trip ID
									</th>
									<th >Pick Up Location
									</th>
									<th  >Pick Up Contact Person
									</th>
									<th  >Pick Up Mobile Number
									</th>
									<th >Drop Location 
									</th>
									<th >Drop Contact Person
									</th>
									<th >Drop Mobile Number
									</th>
									<th >Created
									</th>
									<th >Updated
									</th>
									<th>Actions
									</th>
								  </tr>
								</thead>
								<tbody>
								@php $sn=0; @endphp
								@foreach($data['Requirement'] as $value)
								  <tr role="row" class="odd">
									<td> {{$value->trip_id}}
									</td>
									<td class="sorting_1">{{$value->p_location}}
									</td>
									<td>{{$value->p_contact_person}}
									</td>
									<td>{{$value->p_mob_no}}
									</td>
									<td>{{$value->d_location}}
									</td>
									<td>{{$value->d_contact_person}}
									</td>
									<td>{{$value->d_mob_no}}
									</td>
									<td>{{$value->created_at}}
									</td>
									<td>{{$value->updated_at}}
									</td>
									<td>
									  <a class="btn btn-success" href="{{url('edit-client-requirement/'.$value->id)}}">
										<i class="cil-color-border"></i>
									  </a>
									</td>
								  </tr>
								@endforeach
								</tbody>
							  </table>
							</div>
						  </div>
					  </div>
					</div>
				  </div>
				</div>
            </main>


@endsection