<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/client-creation-form','ClientController@client_creation');
Route::post('/client-creation','ClientController@ClientCreation');
Route::get('/view-clients','ClientController@view_clients');
Route::get('/edit-client-info/{id}','ClientController@edit_client_info');
Route::post('/update-client-creation','ClientController@UpdateClientCreation');
Route::get('/view-client-info/{id}','ClientController@view_client_info');



Route::get('/client-requirement-form','ClientController@client_requirement');
Route::post('/client-requirement','ClientController@ClientRequirement');
Route::get('/view-clients-requirement','ClientController@view_client_requirement');
Route::get('/edit-client-requirement/{id}','ClientController@edit_client_requirement');
Route::post('/update-clients-requirement','ClientController@update_client_requirement');


Route::post('/get-city','LocationController@get_city');
Route::post('/get-location-name','LocationController@get_location');

Route::get('/trip-id', function () {
    return view('Trip.trip-id');
});

Route::get('/create-trip-form','TripController@create_trip');
Route::post('/create-trip','TripController@CreateTrip');
Route::get('/view-trip','TripController@ViewTrip');
Route::get('/edit-trip/{id}','TripController@EditTrip');
Route::post('/update-trip','TripController@UpdateTrip');
Route::post('/get-trips','TripController@GetTrips');

Route::get('/trip-assignments/{id}','TripController@trip_assignments');
Route::post('/trip-assignment','TripController@CreateTripAssign');

Route::get('/create-lr-file/{id}','TripController@View_Create_LR_File');
Route::get('/edite-lr-file/{id}','TripController@View_Create_LR_File');

Route::post('/add-lr-file','TripController@Create_LR_File');
Route::post('/update-lr-file','TripController@update_LR_File');



Route::post('/reject-trip-assign','TripController@rejectTripAssign');
Route::get('/Download_LR_File/{id}','TripController@Download_LR_File');

Route::get('/partial-trip-closure/{id}','TripController@Trip_Closure');
Route::post('/Trip_Closure_add','TripController@Trip_Closure_add');
Route::get('/view-trip-report','TripController@ViewTripClosureReport');

Route::get('/invoice-file/{id}','TripController@view_invoice_file');

Route::get('/generate-invoice/{id}','TripController@generate_invoice');
Route::post('/create-generate-invoice','TripController@create_generate_invoice');



Route::get('/edit-invoice/{id}','TripController@edit_generate_invoice');
Route::post('/update-generate-invoice','TripController@update_generate_invoice');





Route::get('/view-invoice-report','TripController@view_invoice_report');
Route::get('/view-invoice-report/{id}','TripController@find_invoice_report');
Route::get('/Download_Invoice_File/{id}','TripController@Download_Invoice_File');
Route::get('/ApproveAndReject-invoice-report/{id}/{value}','TripController@invoice_update');


Route::get('/view-b-opportunity-report','ReportController@b_opportunity_report');

Route::get('/view-trip-report','ReportController@trip_report');

Route::get('/view-finance-report','ReportController@FinanceReport');




Route::get('/view-assigned-trip','DriverController@ViewTrip');
Route::post('/upload-bills','DriverController@upload_bills');


Route::get('/view-active-trip','DriverController@ViewTrip');
Route::post('/trip-closure-update','DriverController@trip_closure_update');


Route::get('/view-trip-info','DriverController@ViewTripInfo');
Route::get('/trip-view-info/{id}','DriverController@trip_view_info');
